package messnco.mess_enkrypt.UI

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.provider.MediaStore
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.serialization.ImplicitReflectionSerializer
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.models.MMessage
import messnco.mess_enkrypt.viewControler.groupListControlleur
import messnco.mess_enkrypt.viewControler.groupListControlleur.Companion.grpLC
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import java.time.format.DateTimeFormatter



class MessageListAdapter
internal constructor (private var mContext: Context, private val mMessageList: List<MMessage>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    @ImplicitReflectionSerializer
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val message = messageList.get(position)

        when (holder.itemViewType) {
            VIEW_TYPE_MESSAGE_SENT -> (holder as SentMessageHolder).bind(message)
            VIEW_TYPE_IMAGE_SENT -> (holder as SentMessageHolderImage).bind(message)
            VIEW_TYPE_MESSAGE_RECEIVED -> (holder as ReceivedMessageHolder).bind(message)
            VIEW_TYPE_IMAGE_RECEIVED -> (holder as ReceivedMessageHolderImage).bind(message)
        }
    }

    private val VIEW_TYPE_MESSAGE_SENT = 1
    private val VIEW_TYPE_MESSAGE_RECEIVED = 2
    private val VIEW_TYPE_IMAGE_SENT = 3
    private val VIEW_TYPE_IMAGE_RECEIVED = 4

    private val context: Context? = mContext
    private var messageList: List<MMessage> = mMessageList

    override fun getItemCount(): Int {
        return messageList.size
    }

    // Determines the appropriate ViewType according to the sender of the message.
    override fun getItemViewType(position: Int): Int {
        val message = messageList.get(position)
        if (message.picture.isNotEmpty() && message.isMe)
            return VIEW_TYPE_IMAGE_SENT
        else if (message.picture.isNotEmpty() && !message.isMe) {
            return VIEW_TYPE_IMAGE_RECEIVED
        } else {
            return if (message.isMe) {
                // If the current user is the sender of the message
                VIEW_TYPE_MESSAGE_SENT
            } else {
                // If some other user sent the message
                VIEW_TYPE_MESSAGE_RECEIVED
            }
        }
    }

    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder? {
        val view: View

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.message_fragment_sent, parent, false)
            return SentMessageHolder(view)
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.message_fragment_received, parent, false)
            return ReceivedMessageHolder(view)
        } else if (viewType == VIEW_TYPE_IMAGE_SENT) {
            view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.message_fragment_sent_image, parent, false)
            return SentMessageHolderImage(view)
        } else if (viewType == VIEW_TYPE_IMAGE_RECEIVED) {
            view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.message_fragment_received_image, parent, false)
            return ReceivedMessageHolderImage(view)
        }

        return null
    }

    inner private class ReceivedMessageHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var messageText: TextView
        internal var timeText: TextView
        internal var nameText: TextView
        internal var profileImage: ImageView
        internal var nbClick = 0

        init {
            messageText = itemView.findViewById(R.id.text_message_body)
            timeText = itemView.findViewById(R.id.text_message_time)
            nameText = itemView.findViewById(R.id.text_message_name)
            profileImage = itemView.findViewById(R.id.image_message_profile) as ImageView
        }

        @ImplicitReflectionSerializer
        internal fun bind(message: MMessage) {
            messageText.text = message.content

            //timeText.text = Date(message.createAt).toString()
            nameText.text = message.sender.name

            messageText.setOnClickListener {
                nbClick++
                if (nbClick == 1) {
                    Toast.makeText(context, "Copied !",
                            Toast.LENGTH_SHORT).show()

                }
                val clipboard = context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clip = ClipData.newPlainText("message", messageText.text)
                clipboard.primaryClip = clip
//                grpLC.shareMsg(message.content)

            }

            messageText.setOnLongClickListener {
                Log.d("COO", "I'm on shareMsg ")
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, message.content)
                    type = "text/plain"
                }

                val shareIntent = Intent.createChooser(sendIntent, null)
                mContext.startActivity(shareIntent)
//                context.startActivity(shareIntent)
                true
            }
            // Format the stored timestamp into a readable String using method.
            val format = SimpleDateFormat("HH:mm")

            timeText.text = format.format(message.createAt)
            Log.d("IMGGGGG", message.sender.imageUrl)
            if (message.sender.imageUrl.isNotEmpty()) {
                val imageBytes = Base64.decode(message.sender.imageUrl, 0)
                val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
                profileImage.setImageBitmap(image)
            } else {
                message.sender.imageUrl = "https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png"
                // Insert the profile image from the URL into the ImageView.
                Picasso.with(context)
                        .load(message.sender.imageUrl)
                        .placeholder(R.mipmap.ic_launcher)
                        .into(profileImage)
            }


        }
    }

    inner private class ReceivedMessageHolderImage internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //internal var messageText: TextView
        internal var timeText: TextView
        internal var nameText: TextView
        internal var profileImage: ImageView
        internal var nbClick = 0
        internal var picture: ImageView

        init {
            //messageText = itemView.findViewById(R.id.text_message_body)
            picture = itemView.findViewById(R.id.text_message_body) as ImageView
            timeText = itemView.findViewById(R.id.text_message_time)
            nameText = itemView.findViewById(R.id.text_message_name)
            profileImage = itemView.findViewById(R.id.image_message_profile) as ImageView
        }

        internal fun bind(message: MMessage) {
            //val decodedString = Base64.decode(message.picture, Base64.DEFAULT)
            //val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            //picture.setImageBitmap(decodedByte)
//            picture.imageUrl = message.picture
            // Insert the profile image from the URL into the ImageView.
//            Picasso.with(context)
//                    .load(message.picture)
//                    .placeholder(R.mipmap.ic_launcher)
//                    .into(picture)
//            val test = Uri.parse(message.picture)
//            val imageStream = context!!.contentResolver.openInputStream(test)
//            val selectedImage = BitmapFactory.decodeStream(imageStream)
            val imageBytes = Base64.decode(message.picture, 0)
            val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            picture.setImageBitmap(image)
//            picture.setImageBitmap(getBitmapFromURL(message.picture))
            // Format the stored timestamp into a readable String using method.
            val format = SimpleDateFormat("HH:mm")

            timeText.text = format.format(message.createAt)            //timeText.text = Date(message.createAt).toString()
            nameText.text = message.sender.name

            if (message.sender.imageUrl.isNotEmpty()) {
                val imageBytes2 = Base64.decode(message.sender.imageUrl, 0)
                val image2 = BitmapFactory.decodeByteArray(imageBytes2, 0, imageBytes2.size)
                profileImage.setImageBitmap(image2)
            } else {
                message.sender.imageUrl = "https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png"
                // Insert the profile image from the URL into the ImageView.
                Picasso.with(context)
                        .load(message.sender.imageUrl)
                        .placeholder(R.mipmap.ic_launcher)
                        .into(profileImage)
            }
            picture.setOnLongClickListener {
                Log.d("COO", "I'm on shareMsg ")
                val shareIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
//                putExtra(Intent.EXTRA_TEXT, "Sharing your QRcode to trustfull contacts")

                    putExtra(Intent.EXTRA_STREAM, getImageUriFromBitmap(context!!, image))
                    putExtra(Intent.EXTRA_TITLE, "Sharing your image")
                    type = "image/jpeg"
//                type = "text/plain"
                }
                context!!.startActivity(Intent.createChooser(shareIntent, "Remote Destroy QR code"))
//                context.startActivity(shareIntent)
                true
            }
//
//            message.sender.imageUrl = "https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png"
//            // Insert the profile image from the URL into the ImageView.
//            Picasso.with(context)
//                    .load(message.sender.imageUrl)
//                    .placeholder(R.mipmap.ic_launcher)
//                    .into(profileImage)

        }
    }

    inner private class SentMessageHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var messageText: TextView
        internal var timeText: TextView
        internal var nbClick = 0
        init {
            messageText = itemView.findViewById(R.id.text_message_body)
            timeText = itemView.findViewById(R.id.text_message_time)
        }

        @ImplicitReflectionSerializer
        internal fun bind(message: MMessage) {
            messageText.text = message.content

            messageText.setOnClickListener {
                nbClick++
                if (nbClick == 1) {
                    Toast.makeText(context, "Copied !",
                            Toast.LENGTH_SHORT).show()

                }
                val clipboard = context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clip = ClipData.newPlainText("message", messageText.text)
                clipboard.primaryClip = clip

            }

            messageText.setOnLongClickListener {
                Log.d("COO", "I'm on shareMsg ")
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, message.content)
                    type = "text/plain"
                }

                val shareIntent = Intent.createChooser(sendIntent, null)
                mContext.startActivity(shareIntent)
//                context.startActivity(shareIntent)
                true
            }
            // Format the stored timestamp into a readable String using method.
            val format = SimpleDateFormat("HH:mm")

            timeText.text = format.format(message.createAt)
        }
    }

    inner private class SentMessageHolderImage internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var messageText: ImageView
        internal var timeText: TextView
        internal var nbClick = 0
        init {

            messageText = itemView.findViewById(R.id.text_message_body) as ImageView
            timeText = itemView.findViewById(R.id.text_message_time)
        }

        internal fun bind(message: MMessage) {
            //messageText.bi = message.content
//            val decodedString = Base64.decode(message.picture, Base64.DEFAULT)
//            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

//            val test = Uri.parse(message.picture)
//            val imageStream = context!!.contentResolver.openInputStream(test)
//            val selectedImage = BitmapFactory.decodeStream(imageStream)
            val imageBytes = Base64.decode(message.picture, 0)
            val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            messageText.setImageBitmap(image)

            messageText.setOnLongClickListener {
                Log.d("COO", "I'm on shareMsg ")
                val shareIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
//                putExtra(Intent.EXTRA_TEXT, "Sharing your QRcode to trustfull contacts")

                    putExtra(Intent.EXTRA_STREAM, getImageUriFromBitmap(context!!, image))
                    putExtra(Intent.EXTRA_TITLE, "Sharing your image")
                    type = "image/jpeg"
//                type = "text/plain"
                }
                context!!.startActivity(Intent.createChooser(shareIntent, "Remote Destroy QR code"))
//                context.startActivity(shareIntent)
                true
            }
//            Picasso.with(context)
//                    .load(decodedByte)
//                    .placeholder(R.mipmap.ic_launcher)
//                    .into(messageText)

//            val decodedString = Base64.decode(message.picture, Base64.DEFAULT)
//            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
//            messageText.setImageBitmap(decodedByte)

//            Picasso.with(context)
//                    .load(Uri.parse(message.picture))
//                    .placeholder(R.mipmap.ic_launcher)
//                    .into(messageText)

//            messageText.setOnClickListener {
//                nbClick++
//                if (nbClick == 1) {
//                    Toast.makeText(context, "Copied !",
//                            Toast.LENGTH_SHORT).show()
//
//                }
//                if (nbClick % 2 == 1) {
//                    timeText.text = Date(message.createAt).toString()
//                } else {
//                    timeText.text = ""
//                }
//                val clipboard = context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
//                val clip = ClipData.newPlainText("message", messageText.text)
//                clipboard.primaryClip = clip
//            }
            // Format the stored timestamp into a readable String using method.
            val format = SimpleDateFormat("HH:mm")

            timeText.text = format.format(message.createAt)
        }
    }

    fun  downloadImage(url: String ): Bitmap {
        lateinit var bitmap: Bitmap
        lateinit var stream: InputStream
        val bmOptions: BitmapFactory.Options = BitmapFactory.Options();
        bmOptions.inSampleSize = 1;

        try {
            stream = getHttpConnection(url)!!;
            bitmap = BitmapFactory.decodeStream(stream, null, bmOptions);
            stream.close();
        }
        catch (e1: IOException) {
            e1.printStackTrace();
            System.out.println("downloadImage"+ e1.toString());
        }
        return bitmap;
    }

  // Makes HttpURLConnection and returns InputStream

    fun getHttpConnection(urlString: String) : InputStream? {

        val stream: InputStream
        val url = URL(urlString)
        val connection = url.openConnection();
        Log.d("COO", "In the getHttpConnection")
        try {
            val httpConnection: HttpURLConnection = connection as (HttpURLConnection)
            httpConnection.setRequestMethod("GET");
            httpConnection.connect();
            Log.d("COO", "After Connect")

            if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                Log.d("COO", "In the if")
                stream = httpConnection.getInputStream();
                return stream

            }
        }
        catch (ex: Exception) {
            ex.printStackTrace();
            System.out.println("downloadImage" + ex.toString());
        }
        return null
    }

    fun getBitmapFromURL(src: String): Bitmap? {
        try {
            val url = URL(src)
            val connection = url.openConnection() as HttpURLConnection
            connection.setDoInput(true)
            connection.connect()
            val input = connection.getInputStream()
            return BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }

    }

    fun getImageUriFromBitmap(context: Context, bitmap: Bitmap): Uri{
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, "Share this picture", null)
        return Uri.parse(path.toString())
    }
}

//private class AsyncGettingBitmapFromUrl : AsyncTask<String, Void, Bitmap>() {
//
//
//    override fun doInBackground(vararg params: String): Bitmap? {
//
//        println("doInBackground")
//
//        var bitmap: Bitmap? = null
//
//        //bitmap = SentMessageHolderImage.downloadImage(params[0])
//
//        return bitmap
//    }
//
//    override fun onPostExecute(bitmap: Bitmap) {
//
//        println("bitmap$bitmap")
//
//    }
//}