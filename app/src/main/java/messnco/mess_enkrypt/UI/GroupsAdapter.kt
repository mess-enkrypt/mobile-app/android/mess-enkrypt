package messnco.mess_enkrypt.UI

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.serialization.ImplicitReflectionSerializer
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.models.Group
import messnco.mess_enkrypt.models.SharedPref
import messnco.mess_enkrypt.viewControler.groupListControlleur

@ImplicitReflectionSerializer
class GroupsAdapter(private val context: Context,
                    private val dataSource: ArrayList<Group>) : BaseAdapter(){
    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater



    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder

        // 1
        if (convertView == null) {

            // 2
            view = inflater.inflate(R.layout.fragment_grouplist, parent, false)

            // 3
            holder = ViewHolder()
            holder.thumbnailImageView = view.findViewById(R.id.groupPic) as ImageView
            holder.titleTextView = view.findViewById(R.id.groupName) as TextView
            holder.subtitleTextView = view.findViewById(R.id.lastMessage) as TextView

            // 4
            view.tag = holder
        } else {
            // 5
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        // 6
        val titleTextView = holder.titleTextView
        val lastMessage = holder.subtitleTextView
        val thumbnailImageView = holder.thumbnailImageView

        val group = getItem(position) as Group

        titleTextView.text = group.groupName
        if (group.messages.size > 0) {
            lastMessage.text = group.messages[group.messages.size - 1].content
        }

//        Picasso
//                .with(context)
//                .load("https://cdn.pixabay.com/photo/2016/12/18/12/49/cyber-security-1915626_1280.png")
//                .placeholder(R.mipmap.ic_launcher)
//                .into(thumbnailImageView)
        if (group.isFav) {
            Picasso
                    .with(context)
                    .load("https://img.icons8.com/officel/2x/star.png")
                    .placeholder(R.mipmap.ic_launcher)
                    .into(thumbnailImageView)
        } else {
            Picasso
                    .with(context)
                    .load("https://image.flaticon.com/icons/png/512/13/13595.png")
                    .placeholder(R.mipmap.ic_launcher)
                    .into(thumbnailImageView)

        }

        thumbnailImageView.setOnClickListener {
            group.isFav = !group.isFav
            SharedPref.saveGroups(context)
            notifyDataSetChanged()
        }

        //https://image.flaticon.com/icons/png/512/13/13595.png

        return view
    }

    private class ViewHolder {
        lateinit var titleTextView: TextView
        lateinit var subtitleTextView: TextView
        lateinit var thumbnailImageView: ImageView
    }
}