package messnco.mess_enkrypt.UI

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_group_create.*
import kotlinx.android.synthetic.main.fragment_group_create.view.*
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.json
import kotlinx.serialization.stringify
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.models.*
import net.kibotu.pgp.Pgp
import kotlin.random.Random
import android.support.v4.app.FragmentActivity
import android.util.Base64
import messnco.mess_enkrypt.viewControler.*
import messnco.mess_enkrypt.viewControler.userControlleur.Companion.ContactContext


class UsersAdapter(private val context: Context,
                   private val dataSource: ArrayList<User>) : BaseAdapter(){
    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @ImplicitReflectionSerializer
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder

        // 1
        if (convertView == null) {

            // 2
            view = inflater.inflate(R.layout.fragment_user, parent, false)

            // 3
            holder = ViewHolder()
            holder.thumbnailImageView = view.findViewById(R.id.userPic) as ImageView
            holder.titleTextView = view.findViewById(R.id.userName) as TextView
            holder.btnRemoveCtn = view.findViewById(R.id.btnRemoveCtn) as ImageButton
            holder.startConv = view.findViewById(R.id.startConv) as ImageButton

            // 4
            view.tag = holder
        } else {
            // 5
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        // 6
        val titleTextView = holder.titleTextView
        val thumbnailImageView = holder.thumbnailImageView
        val user = getItem(position) as User

        titleTextView.text = user.name
        holder.startConv.setOnClickListener {
            try {
                var isAlready = "false"
                Log.d("COO", "I clicked on an Item")
                var x = 0
                Group.groupList.forEach{
                    if (it.groupName == user.name) {
                        Log.d("COO", "Group already created !")
                        isAlready = "true"
                        userControlleur.moveToGrp(x)

                        val intent = Intent(context, groupListControlleur::class.java)
                        intent.putExtra("pos", x)
                        intent.putExtra("text", "NO")

                        ContactContext.applicationContext.startActivity(intent)

//                        val intent = Intent(context, groupListControlleur::class.java)
//
//                        ContactContext.applicationContext.startActivity(intent)
/*                        val ft = context.supportFragmentManager.beginTransaction()
                        val args = Bundle()
                        groupFragm = groupMainFragment()
                        args.putInt("position", x)

                        groupFragm.arguments = args
                        ft.replace(R.id.userListAct, groupFragm)
                        ft.addToBackStack(groupFragm.toString())
                        ft.commit()
*/

                    }
                    x++
                }

                if (isAlready == "false") {
                    val name = user.name
                    //val name: String = adapterView.selectedItem. titleTextView.text.toString()
                    if (name == "")
                        throw (IllegalArgumentException("No Group Name"))


                    val test = ArrayList<User>()
                    test.add(user)
                    // new class group
                    val admin = ArrayList<User>()
                    admin.add(User.profil)

                    val rand : Int = Random.nextInt()
                    val newGroup = Group(
                            name,
                            rand,
                            "",
                            0,
                            test,
                            admin,
                            ArrayList<MMessage>()
                    )


                    Group.groupList.add(newGroup)


                    val groupUserList = ArrayList<String>()
                    newGroup.members.forEach(){
                        groupUserList.add(it.name)
                    }
                    groupUserList.add(User.profil.name)

                    val sendObj = json{
                        "type" to 1
                        "grpName" to User.profil.name
                        "groupPic" to newGroup.groupPic
                        "creator"  to User.profil.name
                        "gid" to newGroup.id
                        "userlist" to Json.stringify(groupUserList)
                    }
                    newGroup.members.forEach() {
                        Log.d("COO", "Dans le foreach")
                        if (it.name != User.profil.name) {
                            Log.d("COO", "J'emit")
                            Pgp.setPublicKey(it.publicKey)
                            val enkSendObj = Pgp.encrypt(sendObj.toString())
                            CommunicationManager.comManager.h.emit("message:send", it.name, enkSendObj)
                        }
                    }
                    SharedPref.saveGroups(context)
                    val intent = Intent(context, groupListControlleur::class.java)
                    intent.putExtra("pos", Group.groupList.size - 1)
                    intent.putExtra("text", "NO")

                    ContactContext.applicationContext.startActivity(intent)

/*
                    val ft = supportFragmentManager.beginTransaction()
                    //val ft = this.ContactContext.supportFragmentManager.beginTransaction()
                    val args : Bundle = Bundle()
                    groupFragm = groupMainFragment()
                    args.putInt("position", i)
                    Log.d("COO", "Je suis dans le GoToGroup")

                    groupFragm.arguments = args
                    ft.replace(R.id.userListAct, groupFragm)
                    ft.addToBackStack(groupFragm.toString())
                    ft.commit()

 */

                }

/*
                //goToGroup(Conversation(), i)
                val ft = supportFragmentManager.beginTransaction()
                //val ft = this.ContactContext.supportFragmentManager.beginTransaction()
                val args : Bundle = Bundle()
                groupFragm = groupMainFragment()
                args.putInt("position", i)
                Log.d("COO", "Je suis dans le GoToGroup")

                groupFragm.arguments = args
                ft.replace(R.id.userListAct, groupFragm)
                ft.addToBackStack(groupFragm.toString())
                ft.commit()

 */
            } catch (e: Exception) {
                Log.d("Log ", "Exception " + e.toString())
                Toast.makeText(context, "You cannot create a group yet, please fill all the informations !", Toast.LENGTH_SHORT).show()

            }
        }

        holder.btnRemoveCtn.setOnClickListener {
            val alertDialog = AlertDialog.Builder(context).create()
            alertDialog.setTitle("Remove this contact")
            val name = user.name
            val fulls = "Are you sure you want to delete $name ?"
            alertDialog.setMessage(fulls)
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes"
            ) { dialog, _ -> dialog.dismiss() }

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No"
            ) { dialog, _ -> dialog.dismiss() }
            alertDialog.show()

            val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
            val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

            val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
            layoutParams.weight = 10f
            btnPositive.layoutParams = layoutParams
            btnNegative.layoutParams = layoutParams

            btnPositive.setOnClickListener {
                User.deleteUser(user)
                SharedPref.saveContacts(context)
                notifyDataSetChanged()
                alertDialog.dismiss()
            }
        }

        if (user.imageUrl.isNotEmpty()) {
            val imageBytes = Base64.decode(user.imageUrl, 0)
            val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            thumbnailImageView.setImageBitmap(image)
        } else {
            Picasso
                    .with(context)
                    .load("https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png")
                    .placeholder(R.mipmap.ic_launcher)
                    .into(thumbnailImageView)
        }



        return view
    }

    private class ViewHolder {
        lateinit var titleTextView: TextView
        lateinit var thumbnailImageView: ImageView
        lateinit var btnRemoveCtn: ImageButton
        lateinit var startConv: ImageButton

    }
}



class UserRecycleView // data is passed into the constructor
internal constructor(context: Context, private val mData: ArrayList<User>, private val isRemovable: Boolean, fClickListener: ItemClickListener? = null) : RecyclerView.Adapter<UserRecycleView.ViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var mClickListener: ItemClickListener? = fClickListener
    private val cont = context

    // inflates the row layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = mInflater.inflate(R.layout.fragment_user_group, parent, false)
        return ViewHolder(view)
    }

    // binds the data to the TextView in each row
    @ImplicitReflectionSerializer
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = mData[position]
        holder.titleTextView.text = user.name
        if (isRemovable) {
            holder.btnRemoveCtnG.setOnClickListener {
                val alertDialog = AlertDialog.Builder(cont).create()
                alertDialog.setTitle("Remove this contact")
                val name = user.name
                val fulls = "Are you sure you want to delete $name ?"
                alertDialog.setMessage(fulls)
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes"
                ) { dialog, _ -> dialog.dismiss() }

                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No"
                ) { dialog, _ -> dialog.dismiss() }
                alertDialog.show()

                val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

                val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
                layoutParams.weight = 10f
                btnPositive.layoutParams = layoutParams
                btnNegative.layoutParams = layoutParams

                btnPositive.setOnClickListener {
                    groupCreate.newGroupContactList.remove(user)
                    notifyDataSetChanged()
                    alertDialog.dismiss()
                }
            }

        } else {
            holder.btnRemoveCtnG.visibility = View.INVISIBLE
        }

        if (user.imageUrl.isNotEmpty()) {
            val imageBytes = Base64.decode(user.imageUrl, 0)
            val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            holder.thumbnailImageView.setImageBitmap(image)
        } else {
            Picasso
                    .with(cont)
                    .load("https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png")
                    .placeholder(R.mipmap.ic_launcher)
                    .into(holder.thumbnailImageView)
        }
//
//        if (user.imageUrl.length < 0) {
//            Picasso.with(cont)
//                    .load(user.imageUrl)
//                    .placeholder(R.mipmap.ic_launcher)
//                    .into(holder.thumbnailImageView)
//        } else {
//            Picasso.with(cont)
//                    .load("https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png")
//                    .placeholder(R.mipmap.ic_launcher)
//                    .into(holder.thumbnailImageView)
//
//        }
    }

    // total number of rows
    override fun getItemCount(): Int {
        return mData.size
    }


    // stores and recycles views as they are scrolled off screen
    inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        internal var titleTextView: TextView
        internal var thumbnailImageView : ImageView
        internal var btnRemoveCtnG : ImageButton

        init {
            thumbnailImageView = itemView.findViewById(R.id.userPic) as ImageView
            titleTextView = itemView.findViewById(R.id.userName) as TextView
            btnRemoveCtnG = itemView.findViewById(R.id.btnRemoveCtnG) as ImageButton
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            if (mClickListener != null) mClickListener!!.onItemClick(view, adapterPosition)
        }
    }

    // convenience method for getting data at click position
    internal fun getItem(id: Int): User {
        return mData[id]
    }

    // allows clicks events to be caught
    internal fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View, position: Int) {
            Log.d("Log ", "POSITION : $position")
        }
    }

}