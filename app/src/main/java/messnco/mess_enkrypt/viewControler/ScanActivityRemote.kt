package messnco.mess_enkrypt.viewControler

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_profil_page.*
import kotlinx.android.synthetic.main.fragment_remote_destroy.*
import kotlinx.serialization.ImplicitReflectionSerializer
import me.dm7.barcodescanner.zxing.ZXingScannerView
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.models.User

class ScanActivityRemote : AppCompatActivity(), ZXingScannerView.ResultHandler {

    private var mScannerView: ZXingScannerView? = null

    @ImplicitReflectionSerializer
    public override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        groupListControlleur.GrpContext = this
        mScannerView = ZXingScannerView(this)   // Programmatically initialize the scanner view
        setContentView(mScannerView)                // Set the scanner view as the content view
    }

    public override fun onResume() {
        super.onResume()
        mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView!!.startCamera()          // Start camera on resume
    }

    public override fun onPause() {
        super.onPause()
        mScannerView!!.stopCamera()           // Stop camera on pause
    }

    @ImplicitReflectionSerializer
    override fun handleResult(rawResult: Result) {
        // Do something with the result here
        //User.addUser(rawResult.getText(), this)
        Log.v("COO", rawResult.getText()); // Prints scan results
        // Log.v("tag", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)
        remoteDestroyFragment.remotePassW = rawResult.text
        finish()
        // If you would like to resume scanning, call this method below:
        //mScannerView!!.resumeCameraPreview(this);
    }

    @ImplicitReflectionSerializer
    override fun onBackPressed() {
        val intent = Intent(this, remoteDestroyFragment::class.java)
        startActivity(intent)
    }
}