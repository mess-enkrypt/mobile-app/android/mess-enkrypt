package messnco.mess_enkrypt.viewControler

import android.Manifest
import android.Manifest.permission.CAMERA
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import kotlinx.android.synthetic.main.activity_user.*
import kotlinx.android.synthetic.main.fragment_user.view.*
import kotlinx.android.synthetic.main.fragment_user_down.*
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.UnstableDefault
import messnco.mess_enkrypt.Listener.OnSwipeTouchListener
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.UI.UsersAdapter
import messnco.mess_enkrypt.models.User
import java.io.ByteArrayOutputStream

@ImplicitReflectionSerializer
class userControlleur : AppCompatActivity() {

    private lateinit var listView : ListView

    companion object{
        lateinit var contactView : ListView
        lateinit var ContactContext : Context
        lateinit var uA : UsersAdapter
        val QRcodeWidth = 500
        private val IMAGE_DIRECTORY = "/MessEnkrypt"

        fun moveToGrp(x: Int) {

        }
    }

    override fun onResume() {
        super.onResume()
        updateList()
    }


    @UnstableDefault
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        groupListControlleur.GrpContext = this

        listView = userListView
        contactView = listView
        ContactContext = this

        updateList()


        addContactButton.setOnClickListener{
            val intent = Intent(this, addContact::class.java)
            startActivity(intent)
        }

        imageButton.setOnClickListener{
//            val intent = Intent(this, groupListControlleur::class.java)
//            startActivity(intent)
            finish()
        }

        scanQR.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                        PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.CAMERA);
                    //show popup to request runtime permission
                    requestPermissions(permissions, 200);
                }
                else{
                    //permission already granted
                    //pickImageFromGallery();
                    val intent = Intent(this, ScanActivity::class.java)
                    startActivity(intent)
                }
            }
            else{
                //system OS is < Marshmallow
               // pickImageFromGallery();
                val intent = Intent(this, ScanActivity::class.java)
                startActivity(intent)
            }
        }

        QrGen.setOnClickListener {
            bim()
            //Log.d("COO", "Before setVisibility")
            //pbQR.visibility = View.VISIBLE
            //Toast.makeText(this, "Your QR Code is generating ! Please wait a little !", Toast.LENGTH_SHORT).show()
            //val profilPrefs = context.getSharedPreferences(Constants.profilFileName, 0)
            //toGen()
            //val pass = profilPrefs.getString(Constants.pass, "")
        }

        userListView.setOnTouchListener(object : OnSwipeTouchListener() {
            override fun onSwipeLeft() {
                Log.d("ViewSwipe", "Left")
                val intent = Intent(ContactContext, groupListControlleur::class.java)
                startActivity(intent)
            }

//            override fun onSwipeRight() {
//                Log.d("ViewSwipe", "Right")
//                val intent = Intent(pageCtx, groupListControlleur::class.java)
//                startActivity(intent)
//            }
        })

    }

    fun bim() {
    lateinit var bitmap: Bitmap
        Thread(Runnable {

            runOnUiThread(Runnable {
                pbQR.visibility = View.VISIBLE
            })

            // performing some dummy time taking operation
            try {
                bitmap = TextToImageEncode(User.profil.name)!!
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

            // when the task is completed, make progressBar gone
            runOnUiThread(Runnable {
                val builder = AlertDialog.Builder(this)
                val inflater = layoutInflater
                builder.setTitle("Your QRCode")
                val dialogLayout = inflater.inflate(R.layout.alert_dialog_with_picture, null)
                builder.setView(dialogLayout)
                val editText  = dialogLayout.findViewById<ImageView>(R.id.imageView2)
                editText!!.setImageBitmap(bitmap)
                builder.setPositiveButton("OK") { dialogInterface, i ->
                        pbQR.visibility = View.GONE
                }
                builder.setNeutralButton("Share"){_,_ ->
                    pbQR.visibility = View.GONE
                    val lT = this
                    val shareIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
//                putExtra(Intent.EXTRA_TEXT, "Sharing your QRcode to trustfull contacts")

                        putExtra(Intent.EXTRA_STREAM, getImageUriFromBitmap(lT, bitmap))
                        putExtra(Intent.EXTRA_TITLE, "Sharing your QRcode to trustfull contacts")
                        type = "image/jpeg"
//                type = "text/plain"
                    }
                    startActivity(Intent.createChooser(shareIntent, "Remote Destroy QR code"))
                }
                builder.show()
                pbQR.visibility = View.GONE

            })
        }).start()
    }

    fun toGen() {
        val bitmap = TextToImageEncode(User.profil.name)
        val builder = AlertDialog.Builder(this)
        val inflater = layoutInflater
        builder.setTitle("Your QRCode")
        val dialogLayout = inflater.inflate(R.layout.alert_dialog_with_picture, null)
        val editText  = dialogLayout.findViewById<ImageView>(R.id.imageView2)
        editText!!.setImageBitmap(bitmap)

        builder.setView(dialogLayout)
        builder.setPositiveButton("OK") { dialogInterface, i ->
            Log.d("COO", "After setVisibility")
            pbQR.visibility = View.GONE
            onBackPressed()
        }
        builder.show()
    }

    fun getImageUriFromBitmap(context: Context, bitmap: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, "Remote Destroy QRCode", null)
        return Uri.parse(path.toString())
    }

    @Throws(WriterException::class)
    fun TextToImageEncode(Value: String): Bitmap? {
        val bitMatrix: BitMatrix
        try {
            bitMatrix = MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            )

        } catch (Illegalargumentexception: IllegalArgumentException) {

            return null
        }

        val bitMatrixWidth = bitMatrix.getWidth()

        val bitMatrixHeight = bitMatrix.getHeight()

        val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)

        for (y in 0 until bitMatrixHeight) {
            val offset = y * bitMatrixWidth

            for (x in 0 until bitMatrixWidth) {

                pixels[offset + x] = if (bitMatrix.get(x, y))
                    resources.getColor(R.color.black)
                else
                    resources.getColor(R.color.white)
            }
        }
        val bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight)
        return bitmap
    }

    @UnstableDefault
    fun updateList() {
        var instance = User.contacts
        instance.sortWith(compareBy({it.name}))
        val adapter = UsersAdapter(this, instance)
        uA = adapter
        listView.adapter = adapter

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            200 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, ScanActivity::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(this, "Permission Not Granted", Toast.LENGTH_SHORT).show()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

}