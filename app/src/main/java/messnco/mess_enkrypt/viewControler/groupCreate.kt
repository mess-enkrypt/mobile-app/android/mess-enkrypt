package messnco.mess_enkrypt.viewControler

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Notification
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat.checkSelfPermission
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import kotlinx.android.synthetic.main.fragment_group_create.*
import kotlinx.android.synthetic.main.fragment_group_create.view.*
import kotlinx.android.synthetic.main.fragment_top_bar_group_create.view.*
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.json
import kotlinx.serialization.stringify
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.UI.UserRecycleView
import messnco.mess_enkrypt.models.*
import messnco.mess_enkrypt.models.CommunicationManager.Companion.comManager
import net.kibotu.pgp.Pgp
import org.json.JSONObject
import kotlin.random.Random


class groupCreate : Fragment() {

    companion object {
        fun newInstance() = groupCreate()
        var newGroupContactList : ArrayList<User> = ArrayList<User>()
        var storeName : String = ""
        //image pick code
        private val IMAGE_PICK_CODE = 1000;
        //Permission code
        private val PERMISSION_CODE = 1001;


        @ImplicitReflectionSerializer
        fun groupToJson(group : Group) : String {
            val newJsonObj = json{
                "groupName" to group.groupName
                "id" to group.id
                "groupPic" to group.groupPic
                "timeDeletion" to group.timeDeletion
                "members" to "{\"users\":" + Json.stringify(group.members)+"}"
                "admins" to "{\"users\":" + Json.stringify(group.admins)+"}"
                "messages" to "{\"messages\":" + Json.stringify(group.messages)+"}"
                "isActiv" to group.isActiv
                "isFav" to group.isFav
            }

            return newJsonObj.toString()
        }

        @ImplicitReflectionSerializer
        fun groupListToJsonR(groups : ArrayList<Group>) : String{
            var jsonResult = ""
            jsonResult += "{\"groups\":["
            for (i in 0 until groups.size - 1){

                jsonResult += groupToJson(groups[i]) + ","
            }
            if (groups.size > 1) {
                jsonResult += groupToJson(groups[groups.size - 1]) + "]}"
            }

            return (jsonResult)
        }

        @ImplicitReflectionSerializer
        fun groupListToJson(groups : ArrayList<Group>) : String{
            var jsonResult = ""
            jsonResult += "{\"groups\":["
            for (i in 0 until groups.size - 1){

                jsonResult += groupToJson(groups[i]) + ","
            }
            jsonResult += groupToJson(groups[groups.size - 1]) + "]}"

            return (jsonResult)
        }
    }

    private lateinit var groupFrag : Fragment
    private lateinit var addFrag : Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    @UnstableDefault
    @ImplicitReflectionSerializer
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_group_create, container, false)

        groupListControlleur.GrpContext = context

        val addContact = view.add_contact_button

        val contactList = view.groupContactList_create

        val buttonGroupPicture = view.button_group_picture
        contactList.layoutManager = LinearLayoutManager(activity)
        val adapter = UserRecycleView(activity, newGroupContactList, true)
        contactList.adapter = adapter

        if (storeName.isNotBlank()) {
            Log.d("abon", storeName)
            view.input_group_name.setText(storeName)
        }
        view.backButton_groupCreate.setOnClickListener {
            newGroupContactList.clear()
            storeName = ""
            val goBack = Intent(activity, groupListControlleur::class.java)
            startActivity(goBack)
        }


        buttonGroupPicture.setOnClickListener {
            //check runtime permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE);
                }
                else{
                    //permission already granted
                    pickImageFromGallery();
                }
            }
            else{
                //system OS is < Marshmallow
                pickImageFromGallery();
            }
        }

        addContact.setOnClickListener {
            if (User.contacts.size > 0) {
                showDialog()
            } else {
                Toast.makeText(activity, "You don't have any contact to add !", Toast.LENGTH_SHORT).show()
            }

//            val ft = fragmentManager.beginTransaction()
//            addFrag = choseContactToAddFragment.newInstance()
//
//            if (User.contacts.size > 0) {
//                storeName = input_group_name.text.toString()
//                ft.replace(R.id.group_create_frag, addFrag)
//                ft.addToBackStack(addFrag.toString())
//                ft.commit()
//            } else {
//                Toast.makeText(activity, "You don't have any contact to add !", Toast.LENGTH_SHORT).show()
//            }
        }


        view.groupCreateButton2.setOnClickListener {
            Log.d("COO", view.group_picture.toString())
            try {
                val name: String = input_group_name.text.toString()
                if (name == "")
                    throw (IllegalArgumentException("No Group Name"))
                if (newGroupContactList.size == 0)
                    throw (IllegalArgumentException("No User in group"))


                // new class group
                val admin = ArrayList<User>()
                admin.add(User.profil)

                val rand : Int = Random.nextInt()
                val newGroup = Group(
                        name,
                        rand,
                        "",
                        0,
                        newGroupContactList.clone() as ArrayList<User>,
                        admin,
                        ArrayList<MMessage>(),
                        true,
                        false
                )


                Group.groupList.add(0, newGroup)


                val groupUserList = ArrayList<String>()
                newGroup.members.forEach(){
                    groupUserList.add(it.name)
                }
                groupUserList.add(User.profil.name)

                val sendObj = json{
                        "type" to 1
                        "grpName" to newGroup.groupName
                        "groupPic" to newGroup.groupPic
                        "creator"  to User.profil.name
                        "gid" to newGroup.id
                        "userlist" to Json.stringify(groupUserList)
                    }
                newGroup.members.forEach() {
                    Log.d("COO", "Dans le foreach")
                    if (it.name != User.profil.name) {
                        Log.d("COO", "J'emit")
                        Pgp.setPublicKey(it.publicKey)
                        val enkSendObj = Pgp.encrypt(sendObj.toString())
                        comManager.h.emit("message:send", it.name, enkSendObj)
                    }
                }
                SharedPref.saveGroups(context)
                Log.d("COO", groupListToJson(Group.groupList))

                newGroupContactList.clear()
                storeName = ""

                val ft = fragmentManager.beginTransaction()
                val args = Bundle()

                args.putInt("position", SharedPref.getGroupPositionById(newGroup.id))
                //args.putInt("position", Group.groupList.size - 1)

                groupFrag = groupMainFragment()
                groupFrag.arguments = args
                ft.replace(R.id.group_create_frag, groupFrag)
                ft.commit()
            } catch (e: Exception) {
                Log.d("Log ", "Exception " + e.toString())
                Toast.makeText(activity, "You cannot create a group yet, please fill all the informations !", Toast.LENGTH_SHORT).show()

            }
        }

        return view
    }

    // Method to show an alert dialog with multiple choice list items
    private fun showDialog(){
        lateinit var dialog: AlertDialog
        val asc = Array(User.contacts.size) { i -> User.contacts[i].name }
        val ascB = BooleanArray(User.contacts.size) { false }

        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(context)

        // Set a title for alert dialog
        builder.setTitle("Choose contacts to add.")

        /*
            **** reference source developer.android.com ***

            AlertDialog.Builder setMultiChoiceItems (CharSequence[] items,
                            boolean[] checkedItems,
                            DialogInterface.OnMultiChoiceClickListener listener)

            Set a list of items to be displayed in the dialog as the content, you will be notified
            of the selected item via the supplied listener. The list will have a check mark
            displayed to the right of the text for each checked item. Clicking on an item
            in the list will not dismiss the dialog. Clicking on a button will dismiss the dialog.

            Parameters
                items CharSequence : the text of the items to be displayed in the list.

                checkedItems boolean : specifies which items are checked. It should be null in which
                    case no items are checked. If non null it must be exactly the same length
                    as the array of items.

                listener DialogInterface.OnMultiChoiceClickListener : notified when an item on the
                    list is clicked. The dialog will not be dismissed when an item is clicked. It
                    will only be dismissed if clicked on a button, if no buttons are supplied
                    it's up to the user to dismiss the dialog.

            Returns
                AlertDialog.Builder This Builder object to allow for chaining of calls to set methods
        */

        // Define multiple choice items for alert dialog


        builder.setMultiChoiceItems(asc, ascB) { _, which, isChecked->
            ascB[which] = isChecked
            val contact = asc[which]

            // Display the clicked item text
            Toast.makeText(context, "$contact Clicked", Toast.LENGTH_SHORT).show()
            //toast("$color clicked.")
        }


        // Set the positive/yes button click listener
        builder.setPositiveButton("OK") { _, pos2 ->
            // Do something when click positive button
            for (i in 0 until User.contacts.size) {
                val check = ascB[i]
                if (check) {
                    newGroupContactList.add(User.contacts[i])
                }
            }
            val contactList = view!!.groupContactList_create
            contactList.layoutManager = LinearLayoutManager(activity)
            val adapter = UserRecycleView(activity, newGroupContactList, true)
            contactList.adapter = adapter
        }

        dialog = builder.create()
        dialog.show()
    }



    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size >0 && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            group_picture.setImageURI(data?.data)
        }
    }
}
