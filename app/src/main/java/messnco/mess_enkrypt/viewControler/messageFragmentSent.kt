package messnco.mess_enkrypt.viewControler

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import messnco.mess_enkrypt.R

class messageFragmentSent : Fragment() {

    companion object {
        fun newInstance() = messageFragmentSent()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.message_fragment_sent, container, false)
    }


}
