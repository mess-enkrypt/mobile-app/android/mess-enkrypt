package messnco.mess_enkrypt.viewControler

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.ActionMode
import android.view.View
import kotlinx.android.synthetic.main.activity_load_screen.*
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.UI.GroupsAdapter
import messnco.mess_enkrypt.UI.UsersAdapter
import messnco.mess_enkrypt.models.CommunicationManager.Companion.comManager
import messnco.mess_enkrypt.models.Constants
import messnco.mess_enkrypt.models.Group
import messnco.mess_enkrypt.models.SharedPref
import messnco.mess_enkrypt.models.User
import java.lang.Thread.sleep
import kotlin.system.exitProcess

@UnstableDefault
class loadScreen : AppCompatActivity() {



    private var PRIVATE_MODE = 0

    override fun onActionModeFinished(mode: ActionMode?) {
        super.onActionModeFinished(mode)
        finish()
    }

    override fun onResume() {
        super.onResume()
        finish()
    }

    @ImplicitReflectionSerializer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_load_screen)

        groupListControlleur.GrpContext = this
        var itKill = Thread() {
           fun run() {
                while(true)
                {
                    if (groupListControlleur.killApp == true)
                    {
                        Log.d("Rtest", "Kill program")
                        finishAffinity()
                        finish()
                        exitProcess(0)
                    }
                }
            }
            run()
        }.start()

        val prefs = this.getSharedPreferences(Constants.profilFileName, PRIVATE_MODE)
        val profilPref = prefs.getString(Constants.profilID, "")
        val isLockPref = prefs.getString(Constants.isLock, "")
        val keysPrefs = this.getSharedPreferences(Constants.keysFileName, PRIVATE_MODE)
        val privateString = keysPrefs.getString(Constants.profilID, "")
        val useFinger = SharedPref.getFingerPrint(this)
        val intentFinger = SharedPref.getIntentFingerPrint(this)

        if (profilPref == "" && privateString == "") {

            val background = object : Thread() {
                override fun run() {
                    try {
                        initBar.visibility = View.VISIBLE
                        initBar.progress = 0
                        while (initBar.progress != 100) {
                            initBar.progress += 1
                            sleep(30)
                        }
                        val intent = Intent(baseContext, firstConnection::class.java)
                        // start your next activity
                        startActivity(intent)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
            background.start()
            SharedPref.initContact(this)
            SharedPref.initGroup(this)
            SharedPref.initSetting(this)
        } else if (useFinger == "YES" && intentFinger < 4) {
            Log.d("COO", "Je suis dans le YESSS")
            val finger = Intent(baseContext, FingerPrintActivity::class.java)
            startActivity(finger)
        } else if (SharedPref.getIsUnlockPassSet(this) == "true") {
            val p = Intent(baseContext, PasswordActivity::class.java)
            startActivity(p)
        } else if (isLockPref == "true") {
            Log.d("COO", "Je suis lock")
            val ft = supportFragmentManager.beginTransaction()
            val groupFrag = LockFragment.newInstance()

            ft.replace(R.id.loadScreen, groupFrag)
            ft.addToBackStack(groupFrag.toString())
            ft.commit()
        } else {
            User.profil = Json.parse(User.serializer(), prefs.getString(Constants.profilID, "")!!)

            val userPref = this.getSharedPreferences(Constants.userListFileName, PRIVATE_MODE)
            if (userPref.getString(Constants.userList, "") == "{}")
                User.contacts = ArrayList<User>()
            else
                User.contacts = User.getUsers(userPref.getString(Constants.userList, "")!!)

            val adapter = UsersAdapter(this, User.contacts)
            userControlleur.uA = adapter

            val groupPref = this.getSharedPreferences(Constants.groupListFileName, PRIVATE_MODE)
            if (groupPref.getString(Constants.groupList, "") == "{}")
                Group.groupList = ArrayList<Group>()
            else
                Group.groupList = Group.getGroups(groupPref.getString(Constants.groupList, "")!!)
            comManager.launchReceiver(this, User.profil.name)

            val group = Intent(baseContext, groupListControlleur::class.java)
            group.putExtra("text", "NO")
            // start your next activity
            startActivity(group)
        }
    }
}
