package messnco.mess_enkrypt.viewControler

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import kotlinx.android.synthetic.main.activity_profil_page.*
import kotlinx.android.synthetic.main.fragment_remote_destroy.*
import kotlinx.android.synthetic.main.fragment_remote_destroy.view.*
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.json.Json
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.models.CommunicationManager
import messnco.mess_enkrypt.models.CommunicationManager.Companion.comManager
import messnco.mess_enkrypt.models.Constants
import messnco.mess_enkrypt.models.User
import org.json.JSONArray
import org.json.JSONObject

@ImplicitReflectionSerializer
class remoteDestroyFragment : AppCompatActivity() {

    companion object {
        fun newInstance() = remoteDestroyFragment()
        var remotePassW: String = ""
    }


    override fun onResume() {
        super.onResume()
        if (remotePassW != "") {
            Log.d("COO", "J'ai changer remotePassW")
            remotePassphrase.setText(remotePassW)
            //view.remotePassphrase.setText(remotePassW)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, profilPage::class.java)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_remote_destroy)

        groupListControlleur.GrpContext = this


        if (remotePassW != "") {
            Log.d("COO", "J'ai changer remotePassW")
            remotePassphrase.setText(remotePassW)
        }

        back_profilee.setOnClickListener {
//            val intent = Intent(this, profilPage::class.java)
//            startActivity(intent)
            finish()
        }

        scanQRRemote.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                        PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.CAMERA);
                    //show popup to request runtime permission
                    requestPermissions(permissions, 200);
                }
                else{
                    //permission already granted
                    //pickImageFromGallery();
                    val intent = Intent(this, ScanActivityRemote::class.java)
                    startActivity(intent)
                }
            }
            else{
                //system OS is < Marshmallow
                // pickImageFromGallery();
                val intent = Intent(this, ScanActivityRemote::class.java)
                startActivity(intent)
            }
        }

        button_remote.setOnClickListener {
            //val params = HashMap<String,String>()
            Log.d("COO", "Before on user:destoyr")

            if (remoteName.text.toString().length > 1 && remotePassphrase.text.toString().length > 1) {
                Log.d("COO", "Je suis passé dans le if")
                Log.d("COO", remoteName.text.toString())
                Log.d("COO", remotePassphrase.text.toString())
                comManager.h.emit("user:destroy", remoteName.text.toString(), remotePassphrase.text.toString())
                        .on("user:destroy:success") {
                            Log.d("COO", "on user:destroy")

                            fun call (it: Array<Any>) {
                                this.runOnUiThread(Runnable() {
                                    fun run() {
                                        Toast.makeText(
                                                this,
                                                remoteName.text.toString() + " account have been destroyed",
                                                Toast.LENGTH_SHORT
                                        ).show()
                                        finish()
                                        }
                                    run()
                                })
                            }
                            call(it)

                        }
                        .on("user:destroy:err") {
                            fun call (it: Array<Any>) {
                                this.runOnUiThread(Runnable() {
                                    fun run() {
                                        Log.d("Rtest", "error in Remote Destroy")
                                        Toast.makeText(this, "User Not Found Or Wrong passPhrase", Toast.LENGTH_SHORT).show()
//                                        val receiveMSG = JSONArray(it)
//                                        when (receiveMSG[0].toString()) {
//                                            "USER_NOT_FOUND" -> {
//                                                Log.d("COO", "User not found !")
//                                                Toast.makeText(context, "User not found !.",
//                                                        Toast.LENGTH_SHORT).show()
//                                            }
//                                            "WRONG_PASSWORD" -> {
//                                                Log.d("COO", "Bad destroy PassPhrase !")
//                                                Toast.makeText(context, "Bad destroy PassPhrase !.",
//                                                        Toast.LENGTH_SHORT).show()
//                                            }
//                                            "USER_NOT_ACTIVE" -> {
//                                                Log.d("COO", "User Already Deleted !")
//                                                Toast.makeText(context, "User Already Deleted !",
//                                                        Toast.LENGTH_SHORT).show()
//                                            }
//                                        }
                                    }
                                    run()
                                })
                            }
                            call(it)
                        }
                remotePassphrase.setText("")
                remotePassW = ""
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        this.remotePassphrase.setText("")
    }
}