package messnco.mess_enkrypt.viewControler

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.group_main_layout_fragment.view.*
import kotlinx.android.synthetic.main.group_main_message_bar_fragment.*
import kotlinx.android.synthetic.main.group_main_message_bar_fragment.view.*
import kotlinx.android.synthetic.main.group_main_top_bar_fragment.view.*
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.json
import messnco.mess_enkrypt.UI.MessageListAdapter
import messnco.mess_enkrypt.models.CommunicationManager.Companion.comManager
import messnco.mess_enkrypt.models.User.Companion.profil
import messnco.mess_enkrypt.viewControler.groupCreate.Companion.groupListToJson
import android.os.Build
import android.support.v4.content.ContextCompat.checkSelfPermission
import kotlinx.android.synthetic.main.group_main_tab_selector_fragment.view.*
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.models.*
import net.kibotu.pgp.Pgp
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.concurrent.ThreadLocalRandom
import android.support.v7.widget.LinearSmoothScroller
import android.text.Editable
import android.util.Base64
import android.widget.Toast
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_user.*
import kotlinx.android.synthetic.main.fragment_group_create.*
import kotlinx.serialization.json.JsonObject
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream


class groupMainFragment : Fragment() {

    private var PRIVATE_MODE = 0
    private val IMAGE_PICK_CODE = 1000
    //Permission code
    private val PERMISSION_CODE = 1001
    private var isAFileToSend = "false"
    lateinit var theFile : String

    @ImplicitReflectionSerializer
    companion object {
        fun newInstance() = groupMainFragment()

        lateinit var listMessageView : RecyclerView
        var act : MainActivity = MainActivity()
        var actualGroupId : Int = 0
        lateinit var mA: MessageListAdapter
        var isInit: Int = 0

        fun test(context: Context, id: Int) {
            listMessageView.layoutManager = LinearLayoutManager(context)
            Log.d("COO", id.toString())
            val l = comManager.getGroupInListById(id)
            //val test = CommunicationManager.getGroupInListById()
            val adapterUpdate = MessageListAdapter(context, l.messages)
            listMessageView.scrollToPosition(l.messages.size - 1);
            listMessageView.adapter = adapterUpdate
            adapterUpdate.notifyDataSetChanged()
        }

        fun refreshList(mContext: Context, gid: Int) {
            Log.d("COO", gid.toString())
            Log.d("COO", Group.groupList[gid].messages.toString())

            val adapterUpdate = MessageListAdapter(mContext, Group.groupList[gid].messages)
            adapterUpdate.notifyDataSetChanged()
        }
    }



    var gid : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onResume() {
        super.onResume()
        val adapterUpdate = MessageListAdapter(activity, Group.groupList[gid].messages)
        adapterUpdate.notifyDataSetChanged()

    }
    override fun onPause() {
        super.onPause()
    }


    @ImplicitReflectionSerializer
    @UnstableDefault
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        isInit++
        val view =  inflater.inflate(messnco.mess_enkrypt.R.layout.group_main_layout_fragment, container, false)

        groupListControlleur.GrpContext = context

        val messageList = view.messageList_view
        listMessageView = messageList

        gid = arguments.getInt("position")
        actualGroupId = gid

        messageList.layoutManager = LinearLayoutManager(activity)
        val adapter = MessageListAdapter(activity, Group.groupList[gid].messages)
        //messageList.scrollToPosition(1)
        messageList.scrollToPosition(Group.groupList[gid].messages.size - 1)
        messageList.adapter = adapter
        mA = adapter

        if (arguments.getString("text") != "NO") {
            val lol = arguments.getString("text")
            Log.d("COO", "J'ai reçu $lol")
//            inputText.text = Editable.Factory.getInstance().newEditable(lol)
            view.inputText.setText(lol)
        }

        view.groupName.text = Group.groupList[gid].groupName



        view.sendPic.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE);
                }
                else{
                    //permission already granted
                    pickImageFromGallery();
                }
            }
            else{
                //system OS is < Marshmallow
                pickImageFromGallery();
            }
            Log.d("COO", "IL a clické")
//            Toast.makeText(context, "COMING SOON !", Toast.LENGTH_SHORT).show()
        }

        view.pullToRefresh.setOnRefreshListener {
            @Override
            fun onRefresh() {
                Log.d("COO", "Refreshing List")
                messageList.layoutManager = LinearLayoutManager(activity)
                val adapterUpdate = MessageListAdapter(activity, Group.groupList[gid].messages)
                Log.d("COO", Group.groupList[gid].messages.size.toString())
                //messageList.scrollToPosition(1)
                messageList.scrollToPosition(Group.groupList[gid].messages.size);
                messageList.adapter = adapterUpdate
                adapterUpdate.notifyDataSetChanged()
                view.pullToRefresh.setRefreshing(false);

            }
            onRefresh()
        }



        view.backButton_group.setOnClickListener {
            fragmentManager.popBackStackImmediate()
        }

        view.groupSettings.setOnClickListener {
            val ft = fragmentManager.beginTransaction()

            val args = Bundle()
            Log.d("COO", Group.groupList[gid].toString())
            args.putInt("pos", gid)
            args.putString("members", Group.groupList[gid].members.toString())
            Log.d("COO", args.toString())
            val contactInGroupFrag = contactInGroupFragment()
            contactInGroupFrag.arguments = args
            ft.replace(R.id.groupMainLayout, contactInGroupFrag)
            ft.addToBackStack(contactInGroupFrag.toString())
            ft.commit()
        }

        view.sendButton.setOnClickListener {
            if (inputText.text.isEmpty() && isAFileToSend == "false")
                return@setOnClickListener
//            if (isAFileToSend == "true") {
//                comManager.h.emit("message:upload", theFile)
//                        .on("message:upload:success", Emitter.Listener {
//                            fun call (it: Array<Any>) {
//                                activity.runOnUiThread(Runnable () {
//                                    fun run() {
//                                        val receiveMSG = JSONArray(it)
//                                        //val userToRecord = JSONObject(receiveMSG[0].toString())
//                                        theFile = receiveMSG[0].toString()
//                                    }
//                                    run()
//                                })
//                            }
//                            call(it)
//                        })
//            }
            val myMess = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val date = LocalDateTime.now()
                if (isAFileToSend == "true") {
                    Log.d("COO", theFile)
                    MMessage(Group.groupList[gid].messages.size, inputText.text.toString(), date.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), profil, true, theFile.toString())

                } else {
                    MMessage(Group.groupList[gid].messages.size, inputText.text.toString(), date.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), profil, true, "")
                }
            } else {
                if (isAFileToSend == "true") {
                    MMessage(Group.groupList[gid].messages.size, inputText.text.toString(), ThreadLocalRandom.current().nextLong() ,profil, true, theFile.toString())

                } else {
                    MMessage(Group.groupList[gid].messages.size, inputText.text.toString(), ThreadLocalRandom.current().nextLong() ,profil, true, "")
                }
            }


            Group.groupList[gid].messages.add(myMess)

            Group.groupList[gid].members.forEach() {
                var grpN = Group.groupList[gid].groupName
                if (Group.groupList[gid].groupName == it.name) {// && User.profil.name == Group.groupList[gid].admins[0].name) {
                    grpN = User.profil.name
                }


                // Gestion D'erreur plutot que de crash quand le contact = "no"
                if (it.name == "no")
                {
                    val builder = AlertDialog.Builder(this.context)
                    val inflater = layoutInflater
                    builder.setTitle("Contact Error")
                    builder.setPositiveButton("OK") { dialogInterface, i ->
                    }
                    builder.show()
                    return@setOnClickListener
                }


                val myID = Group.groupList[gid].id

                if (it.name != User.profil.name) {

                    val publicKey = it.publicKey
                    Pgp.setPublicKey(publicKey)

                    Log.d("COO", myMess.content)
                    val encrypted = Pgp.encrypt(myMess.content)
                    Log.d("COO", encrypted)
                    var msgObj : JsonObject
                    if (isAFileToSend == "true") {
                        msgObj = json{
                            "type" to 0
                            "content" to encrypted
                            "grpName" to grpN
                            "grpId" to Group.groupList[gid].id
                            "sender" to User.profil.name
                            "id" to myMess.ID
                            "picture" to theFile
                        }

                    } else {
                         msgObj = json{
                            "type" to 0
                            "content" to encrypted
                            "grpName" to grpN
                            "grpId" to Group.groupList[gid].id
                            "sender" to User.profil.name
                             "id" to myMess.ID
                            "picture" to ""

                        }

                    }
                    Log.d("COO", msgObj.toString())
                    val enkMsgObj = Pgp.encrypt(msgObj.toString())
                    Log.d("COO", "J'emit un message")
                    Log.d("COO", msgObj.toString())
                    comManager.h.emit("message:send", it.name, enkMsgObj).on("info", { Log.d("COO", "info")})
                    SharedPref.saveGroups(context)
                    gid = SharedPref.getGroupPositionById(myID)

                }
            }


            messageList.layoutManager = LinearLayoutManager(activity)
            val adapterUpdate = MessageListAdapter(activity, Group.groupList[gid].messages)
            messageList.scrollToPosition(Group.groupList[gid].messages.size - 1);
            messageList.adapter = adapterUpdate
            adapterUpdate.notifyDataSetChanged()



            // clear input field
            inputText.text.clear()
            sendPic.setImageResource(R.drawable.iconfinder_image01)
        }

        return view
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size >0 && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    //handle result of picked image
    @ImplicitReflectionSerializer
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                val imgUri = data!!.toUri(Intent.URI_ALLOW_UNSAFE)
                val test = Uri.parse(imgUri)
                val imageStream = context.contentResolver.openInputStream(test)
                val selectedImage = BitmapFactory.decodeStream(imageStream)
//                val encodedImage = encodeImage(selectedImage)
//                Log.d("COO", encodedImage)
//                val decodedString = Base64.decode(encodedImage, Base64.DEFAULT)
//                val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                view!!.sendPic.setImageBitmap(selectedImage)
                isAFileToSend = "true"
                theFile = firstConnection.BitMapToString(selectedImage)
//                comManager.h.emit("message:upload", theFile)
//                        .on("message:upload:success", Emitter.Listener {
//                            fun call (it: Array<Any>) {
//                                activity.runOnUiThread(Runnable () {
//                                    fun run() {
//                                        val receiveMSG = JSONArray(it)
//                                        //val userToRecord = JSONObject(receiveMSG[0].toString())
////                                        theFile = receiveMSG[0].toString()
//                                    }
//                                    run()
//                                })
//                            }
//                            call(it)
//                        })
            } else {
                Toast.makeText(context, "Your Android version isn't updated ! You can not use this feature !", Toast.LENGTH_SHORT).show()
            }
            //group_picture.setImageURI(data?.data)
        }
    }

    private fun encodeImage(bm: Bitmap) : String
    {
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos)
        val b = baos.toByteArray()
        val encImage = Base64.encodeToString(b, Base64.DEFAULT)

        return encImage
    }

}
