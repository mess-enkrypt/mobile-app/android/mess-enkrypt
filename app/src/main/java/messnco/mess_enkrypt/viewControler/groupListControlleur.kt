package messnco.mess_enkrypt.viewControler

import android.app.ActivityOptions
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.AdapterView
import android.widget.ListView
import kotlinx.android.synthetic.main.fragment_bottom_grouplist_bar.*
import kotlinx.android.synthetic.main.fragment_navbar.*
import kotlinx.serialization.UnstableDefault
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.UI.GroupsAdapter
import android.support.v7.app.AlertDialog
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_grouplist.*
import kotlinx.serialization.ImplicitReflectionSerializer
import messnco.mess_enkrypt.Listener.OnSwipeTouchListener
import messnco.mess_enkrypt.models.*
import kotlin.system.exitProcess


@UnstableDefault
@ImplicitReflectionSerializer
class groupListControlleur : AppCompatActivity() {

    private lateinit var listView : ListView
    private lateinit var groupFragm : Fragment
    var groupsInstance = Group.groupList
    lateinit var grpAdapter : GroupsAdapter


    companion object {
        lateinit var GrpContext : Context
        lateinit var listGrpView : ListView
        val grpLC: groupListControlleur = groupListControlleur()
        lateinit var gA: GroupsAdapter
        var killApp = false


        fun toDestroy(context: Context) {
            SharedPref.clearSharedPref(context)
            fun call () {
                grpLC.runOnUiThread(Runnable() {
                    fun run() {
                        val alertDialog = AlertDialog.Builder(GrpContext).create()
                        alertDialog.setTitle("Remote Destroy")
                        val fulls = "It looks that you receive a destroy signal"
                        alertDialog.setMessage(fulls)
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK"
                        ) { dialog, which -> dialog.dismiss() }
                        alertDialog.show()

                        val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
                        layoutParams.weight = 10f
                        btnPositive.layoutParams = layoutParams

                        btnPositive.setOnClickListener{
                            killApp = true
                            exitProcess(0)
                        }
                    }
                    run()
                })
            }
            call()
        }

    }

    override fun onBackPressed() {

        val count = supportFragmentManager.backStackEntryCount

        if (count == 0) {
//            super.onBackPressed()
            //additional code
            val alertDialog = AlertDialog.Builder(this).create()

            alertDialog.setTitle("Exit the App")
            val fulls = "Are you sure you want to close MessEnkrypt ?"
            alertDialog.setMessage(fulls)
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes"
            ) { dialog, _ -> finish() }

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No"
            ) { dialog, _ -> dialog.dismiss() }
//
//            val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
//            val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)
//
//            val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
//            layoutParams.weight = 10f
//            btnPositive.layoutParams = layoutParams
//            btnNegative.layoutParams = layoutParams

//            btnPositive.setOnClickListener {
//                finish()
//            }
            alertDialog.show()

        } else {
            supportFragmentManager.popBackStack()
            grpAdapter.notifyDataSetChanged()
            updateList(grpAdapter)
        }
//        super.onBackPressed()

    }

    override fun onRestart() {
        super.onRestart()
        grpAdapter.notifyDataSetChanged()
        updateList(grpAdapter)
    }

    override fun onResume() {
        super.onResume()
        grpAdapter.notifyDataSetChanged()
        updateList(grpAdapter)

    }

    @UnstableDefault
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grouplist)
        if (firstConnection.firstCo == true)
        {
            Log.d("Rtest", "this is the first co")
            firstConnection.firstCo = false
            val i2 = Intent(this, remoteTuto::class.java)
            startActivity(i2)
        }

        listView = this.findViewById(R.id.groupListView)
        listGrpView = listView
        GrpContext = this

        if(intent.getIntExtra("pos", -1) != -1 && intent.getStringExtra("text") == "NO") {
            Log.d("COO", "Il y a une POS $intent.getIntExtra(\"pos\", -1), mais le TEXT est NO => $intent.getStringExtra(\"text\")")
            val ft = supportFragmentManager.beginTransaction()
            val args : Bundle = Bundle()
/*
            args.putString("newGroup", Json.stringify(groupList[position]))
*/
            args.putInt("position", intent.getIntExtra("pos", 0))
            args.putString("text", intent.getStringExtra("text"))

            groupFragm = groupMainFragment()
            groupFragm.arguments = args
            ft.replace(R.id.groupListAct, groupFragm)
            ft.addToBackStack(groupFragm.toString())
            ft.commit()
        } else if (intent.getIntExtra("pos", -1) != -1 && intent.getStringExtra("text") != "NO") {
            Log.d("COO", "Il y a une POS $intent.getIntExtra(\"pos\", -1), mais le TEXT est PAS NO => $intent.getStringExtra(\"text\")")
            val ft = supportFragmentManager.beginTransaction()
            val args : Bundle = Bundle()
/*
            args.putString("newGroup", Json.stringify(groupList[position]))
*/
            args.putInt("position", intent.getIntExtra("pos", 0))
            args.putString("text", intent.getStringExtra("text"))

            groupFragm = groupMainFragment()
            groupFragm.arguments = args
            ft.replace(R.id.groupListAct, groupFragm)
            ft.addToBackStack(groupFragm.toString())
            ft.commit()
        }

        groupCreateButton.setOnClickListener {

                val ft = supportFragmentManager.beginTransaction()
                val groupFrag = groupCreate.newInstance()

                ft.replace(R.id.groupListAct, groupFrag)
                ft.addToBackStack(groupFrag.toString())
                ft.commit()
        }

        listView.setOnTouchListener(object : OnSwipeTouchListener() {
            override fun onSwipeLeft() {
                Log.d("ViewSwipe", "Left")
                val intent = Intent(GrpContext, profilPage::class.java)
                startActivity(intent)
            }

            override fun onSwipeRight() {
                Log.d("ViewSwipe", "Right")
                val intent = Intent(GrpContext, userControlleur::class.java)
                startActivity(intent)
            }
        })

        settings.setOnClickListener {
            val intent = Intent(this, profilPage::class.java)
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        }

        pullToRefreshGroup.setOnRefreshListener {
            @Override
            fun onRefresh() {
                Log.d("COO", "Refreshing List")
//                groupsInstance.sortByDescending { selector(it)}
                Log.d("COO", "J'ai fini de sortby")
                grpAdapter = GroupsAdapter(this, groupsInstance)
                pullToRefreshGroup.setRefreshing(false);

                updateList(grpAdapter)
            }
            onRefresh()
        }

        listView.setOnItemClickListener { _, _, position: Int, _: Long ->

            val ft = supportFragmentManager.beginTransaction()
            val args : Bundle = Bundle()
/*
            args.putString("newGroup", Json.stringify(groupList[position]))
*/
            args.putInt("position", position)
            args.putString("text", "NO")

            groupFragm = groupMainFragment()
            groupFragm.arguments = args
            ft.replace(R.id.groupListAct, groupFragm)
            ft.addToBackStack(groupFragm.toString())
            ft.commit()
        }

        contactButton.setOnClickListener{
            val intent = Intent(this, userControlleur::class.java)
            startActivity(intent)
        }

        groupsInstance.forEach {
            Log.d("COO", "Grp : ${it.groupName} is active ? ${it.isActiv}")
        }
        //Log.d("COO", groupsInstance.toString())
//        groupsInstance.sortByDescending { selector(it)}
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            groupsInstance.removeIf {
                !it.isActiv
            }
        }
//        var test: ArrayList<Group> = groupsInstance
//        for (group in groupsInstance) {
//            if (group.isActiv) {
//                Log.d("COO", "j'add dans test pour les groups inactifs")
//                test.add(group)
//            }
//        }
        Log.d("COO", "J'ai fini de sortby")
        Log.d("COO", groupsInstance.toString())
        grpAdapter = GroupsAdapter(this, groupsInstance)

        updateList(grpAdapter)

        gA = grpAdapter

    }

    private fun handleSendText(intent: Intent) {
        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            // Update UI to reflect text being shared
        }
    }

    fun selector(g: Group): Long{
        Log.d("COO", g.messages.toString())
        if (g.messages.size > 1) {
            val lol = g.messages.last()
            return lol.createAt
        } else {
            return 3L
        }
        //return lol
    }


    @UnstableDefault
    fun updateList(adapter : GroupsAdapter) {
        listView.adapter = adapter
    }

}