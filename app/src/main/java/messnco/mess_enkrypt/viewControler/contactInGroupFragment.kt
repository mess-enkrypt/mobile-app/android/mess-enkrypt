package messnco.mess_enkrypt.viewControler

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_contact_list_view.view.*
import kotlinx.android.synthetic.main.fragment_remote_destroy.*
import kotlinx.android.synthetic.main.fragment_remote_destroy.view.*
import kotlinx.serialization.ImplicitReflectionSerializer
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.UI.UserRecycleView
import messnco.mess_enkrypt.models.CommunicationManager
import messnco.mess_enkrypt.models.Group
import messnco.mess_enkrypt.models.SharedPref
import messnco.mess_enkrypt.models.User
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Test

@ImplicitReflectionSerializer
class contactInGroupFragment : Fragment(){
    companion object {
        fun newInstance() = contactInGroupFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_contact_list_view, container, false)

        groupListControlleur.GrpContext = context

        val recycleView = view.recyclerViewAddContact
        recycleView.layoutManager = LinearLayoutManager(activity)

        val pos = arguments.getInt("pos")
        val adapter = UserRecycleView(activity, Group.groupList[pos].members, false)
        recycleView.adapter = adapter

        view.imageButton.setOnClickListener {
//            val ft = fragmentManager.beginTransaction()
//
//            val toSend = Bundle()
//            toSend.putInt("position", pos)
//            val groupMainf = groupMainFragment()
//            groupMainf.arguments = toSend
//            ft.replace(R.id.contact_list_frag, groupMainf)
            fragmentManager.popBackStackImmediate()
//            ft.addToBackStack(groupMainf.toString())
//            ft.commit()
        }

        view.button_delete_group.setOnClickListener {
            val alertDialog = AlertDialog.Builder(context).create()
            alertDialog.setTitle("Delete this group")
            val name = Group.groupList[pos].groupName
            val fulls = "Are you sure you want to delete $name ?"
            alertDialog.setMessage(fulls)
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes"
            ) { dialog, which -> dialog.dismiss() }

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No"
            ) { dialog, which -> dialog.dismiss() }
            alertDialog.show()

            val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
            val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

            val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
            layoutParams.weight = 10f
            btnPositive.layoutParams = layoutParams
            btnNegative.layoutParams = layoutParams

            btnPositive.setOnClickListener {
                Group.setGroupActiv(pos, false)
                SharedPref.saveGroups(context)
                val goBack = Intent(activity, groupListControlleur::class.java)
                startActivity(goBack)
                alertDialog.dismiss()
            }
        }
        return view
    }
}