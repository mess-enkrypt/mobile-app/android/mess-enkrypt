package messnco.mess_enkrypt.viewControler

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.fragment_remote_tuto.*
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.models.User
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import kotlinx.android.synthetic.main.activity_profil_page.*
import kotlinx.serialization.ImplicitReflectionSerializer
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


@ImplicitReflectionSerializer
class remoteTuto : AppCompatActivity() {
    companion object {
        val QRcodeWidth = 500
        private val IMAGE_DIRECTORY = "/MessEnkrypt"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        groupListControlleur.GrpContext

        setContentView(R.layout.fragment_remote_tuto)
        //val passPhrase = User.profil.passPhrase
        //passphraseValue.text = passPhrase
        //val welcomeI = username.text.toString()
        val name = User.profil.name
        usernameTuto.text = " $name !"

        val bitmap = this.TextToImageEncode(User.profil.passPhrase)
        val lol = findViewById<ImageView>(R.id.destroyImg)
        lol.setImageBitmap(bitmap)

        downicon.setOnClickListener {
            saveImage2(bitmap!!)
            Toast.makeText(this, "QRCode Saved !", Toast.LENGTH_SHORT).show()
        }

        shareicon.setOnClickListener {
            val lT = this
            val shareIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
//                putExtra(Intent.EXTRA_TEXT, "Sharing your QRcode to trustfull contacts")

                putExtra(Intent.EXTRA_STREAM, getImageUriFromBitmap(lT, bitmap!!))
                putExtra(Intent.EXTRA_TITLE, "Sharing your QRcode to trustfull contacts")
                type = "image/jpeg"
//                type = "text/plain"
            }
            startActivity(Intent.createChooser(shareIntent, "Remote Destroy QR code"))
        }

        button_continue.setOnClickListener {
            try {
//                val contactB = Intent(this, groupListControlleur::class.java)
//                contactB.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                contactB.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                contactB.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                startActivity(contactB)
                finish()
            } catch (e: Exception) {
                println(e)
            }
        }

        passphraseValue.setOnClickListener{
            val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("passphrase", passphraseValue.text)
            clipboard.primaryClip = clip
            Toast.makeText(this, "Copied !",
                    Toast.LENGTH_SHORT).show()
        }

    }

    fun getImageUriFromBitmap(context: Context, bitmap: Bitmap): Uri{
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, "Remote Destroy QRCode", null)
        return Uri.parse(path.toString())
    }

    fun saveImage2(bitmap: Bitmap):Uri{
        // Save image to gallery
        val savedImageURL = MediaStore.Images.Media.insertImage(
                contentResolver,
                bitmap,
                "Remote Destroy",
                "QRCode of Remote Destroy"
        )

        // Parse the gallery image url to uri
        return Uri.parse(savedImageURL)
    }

    fun saveImage(myBitmap: Bitmap?): String {
        val bytes = ByteArrayOutputStream()
        myBitmap!!.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
                Environment.getExternalStorageDirectory().toString() + IMAGE_DIRECTORY)
        // have the object build the directory structure, if needed.

        if (!wallpaperDirectory.exists()) {
            Log.d("dirrrrrr", "" + wallpaperDirectory.mkdirs())
            wallpaperDirectory.mkdirs()
        }

        try {
            val f = File(wallpaperDirectory, Calendar.getInstance()
                    .timeInMillis.toString() + ".jpg")
            f.createNewFile()   //give read write permission
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this,
                    arrayOf(f.path),
                    arrayOf("image/jpeg"), null)
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.absolutePath)
            Toast.makeText(this, "QRCode Saved !", Toast.LENGTH_SHORT).show()

            return f.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""

    }

    @Throws(WriterException::class)
    fun TextToImageEncode(Value: String): Bitmap? {
        val bitMatrix: BitMatrix
        try {
            bitMatrix = MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            )

        } catch (Illegalargumentexception: IllegalArgumentException) {

            return null
        }

        val bitMatrixWidth = bitMatrix.getWidth()

        val bitMatrixHeight = bitMatrix.getHeight()

        val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)

        for (y in 0 until bitMatrixHeight) {
            val offset = y * bitMatrixWidth

            for (x in 0 until bitMatrixWidth) {

                pixels[offset + x] = if (bitMatrix.get(x, y))
                    resources.getColor(R.color.purple)
                else
                    resources.getColor(R.color.white)
            }
        }
        val bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight)
        return bitmap
    }

}