package messnco.mess_enkrypt.viewControler

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_contact_list_view.view.*
import kotlinx.serialization.ImplicitReflectionSerializer
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.UI.RecyclerItemClickListenr
import messnco.mess_enkrypt.UI.UserRecycleView
import messnco.mess_enkrypt.models.User
import messnco.mess_enkrypt.viewControler.groupCreate.Companion.newGroupContactList


class choseContactToAddFragment : Fragment() {
    private lateinit var groupFrag : Fragment


    companion object {
        fun newInstance() = choseContactToAddFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @ImplicitReflectionSerializer
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_contact_list_view, container, false)

        groupListControlleur.GrpContext = context

        val recycleView = view.recyclerViewAddContact
        recycleView.layoutManager = LinearLayoutManager(activity)


        val adapter = UserRecycleView(activity, User.contacts, false)
        recycleView.adapter = adapter

        recycleView.addOnItemTouchListener(RecyclerItemClickListenr(activity, recycleView, object : RecyclerItemClickListenr.OnItemClickListener {

            override fun onItemClick(view: View, position: Int) {
                //do your work here..
                if (!newGroupContactList.contains(User.contacts[position])) {
                    newGroupContactList.add(User.contacts[position])
                }


                delete()
            }
            override fun onItemLongClick(view: View?, position: Int) {
                val ft = fragmentManager.beginTransaction()
                groupFrag = groupCreate.newInstance()

                ft.add(R.id.contact_list_frag, groupFrag)
                ft.addToBackStack(groupFrag.toString())
                ft.commit()
            }
        }))


        return view
    }


    fun delete()
    {
        val ft = fragmentManager.beginTransaction()
        groupFrag = groupCreate.newInstance()
        /*ft.addToBackStack(groupFrag.toString())*/
        ft.replace(R.id.contact_list_frag, groupFrag)
        //ft.remove(this)
        ft.commit()
    }
}