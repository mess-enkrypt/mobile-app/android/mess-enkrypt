package messnco.mess_enkrypt.viewControler

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_profil_page.*
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.models.User
import kotlinx.serialization.ImplicitReflectionSerializer
import messnco.mess_enkrypt.Listener.OnSwipeTouchListener
import messnco.mess_enkrypt.models.SharedPref
//import moe.tlaster.kotlinpgp.KotlinPGP
//import moe.tlaster.kotlinpgp.data.EncryptParameter
//import moe.tlaster.kotlinpgp.data.PublicKeyData
//import moe.tlaster.kotlinpgp.data.SignatureData


@ImplicitReflectionSerializer
class profilPage : AppCompatActivity() {

    companion object {
        val QRcodeWidth = 500
        private val IMAGE_DIRECTORY = "/MessEnkrypt"
//        lateinit var pageCtx : Context

    }

    override fun onBackPressed() {

        val count = supportFragmentManager.backStackEntryCount
        button_start_remote.visibility = View.VISIBLE

        if (SharedPref.getIsLock(this) == "true") {
            Toast.makeText(this, "Please Unlock your app !", Toast.LENGTH_SHORT).show()
        }
        else if (count == 0) {
            super.onBackPressed()

        } else {
            button_start_remote.visibility = View.VISIBLE
            supportFragmentManager.popBackStack()

        }
//        super.onBackPressed()

    }

    override fun onResume() {
        super.onResume()
        Log.d("COO", "OnResume ProfilPage")

        username.text = User.profil.name
//        pageCtx = this
        button_start_remote.visibility = View.VISIBLE
        if (SharedPref.getFingerPrint(this) == "YES") {
            switchFP.isChecked = true
        }
        if (SharedPref.getIsUnlockPassSet(this) == "true") {
            switchUnlockPassword.isChecked = true
        }

        if (SharedPref.getIsDestroyPassSet(this) == "true") {
            switchDestroyPassword.isChecked = true
        }

    }

    override fun onPause() {
        super.onPause()
        Log.d("COO", "OnPause ProfilPage")
        button_start_remote.visibility = View.INVISIBLE

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profil_page)

        groupListControlleur.GrpContext = this

        username.text = User.profil.name
//        pageCtx = this
        Log.d("COO", "Askip avant sharedPref bamabm")
        Log.d("COO", SharedPref.getIsUnlockPassSet(this).toString())

        if (User.profil.imageUrl.isNotEmpty()) {
            Log.d("COO", User.profil.imageUrl)
            val imageBytes = Base64.decode(User.profil.imageUrl, 0)
            val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            profil_picture.setImageBitmap(image)
        }

        if (SharedPref.getIsUnlockPassSet(this) == "true") {
            switchUnlockPassword.isChecked = true
        }

        if (SharedPref.getIsDestroyPassSet(this) == "true") {
            switchDestroyPassword.isChecked = true
        }

        if (SharedPref.getFingerPrint(this) == "YES") {
            Log.d("COO", "Askip le sharedPref bamabm")
            switchFP.isChecked = true
        }

        profilPageFragment.setOnTouchListener(object : OnSwipeTouchListener() {
            override fun onSwipeRight() {
                Log.d("ViewSwipe", "Right")
                onBackPressed()
            }
        })

        // retour en à la page précédente
        backButton_profil.setOnClickListener {
//            onBackPressed()
            finish()
        }

        // bouton d'activiation du unlock password
        unlockPassicon.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            val inflater = layoutInflater
            builder.setTitle("Unlock Password")
            val dialogLayout = inflater.inflate(R.layout.alert_dialog_set_unlock_pass, null)
            val editText  = dialogLayout.findViewById<EditText>(R.id.editText)
            builder.setView(dialogLayout)
            builder.setPositiveButton("OK") { dialogInterface, i ->
                if (editText.text.toString() == "")
                {
                    Toast.makeText(applicationContext, "Password cannot be NULL", Toast.LENGTH_SHORT).show()
                }
                else if (editText.text.toString() == SharedPref.getDestroyPass(this))
                {
                    Toast.makeText(applicationContext, "Your UPass must be different", Toast.LENGTH_SHORT).show()
                }
                else
                {
                    Toast.makeText(applicationContext, "Your Upass has been set !", Toast.LENGTH_SHORT).show()
                    SharedPref.saveUnlockPass(this, editText.text.toString())
                    SharedPref.setIsUnlockPassSet(this, "true")
                }
            }
            builder.show()
        }

        // bouton d'activation du destroy password
        destroyPassicon.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            val inflater = layoutInflater
            builder.setTitle("Destroy Password")
            val dialogLayout = inflater.inflate(R.layout.alert_dialog_set_destroy_pass, null)
            val editText  = dialogLayout.findViewById<EditText>(R.id.editTextDestroy)
            builder.setView(dialogLayout)
            builder.setPositiveButton("OK") { dialogInterface, i ->
                if (editText.text.toString() == "")
                {
                    Toast.makeText(applicationContext, "Password cannot be NULL", Toast.LENGTH_SHORT).show()
                }
                else if (editText.text.toString() == SharedPref.getUnlockPass(this))
                {
                    Toast.makeText(applicationContext, "Your DPass must be different", Toast.LENGTH_SHORT).show()
                }
                else
                {
                    Toast.makeText(applicationContext, "Your Dpass has been set !", Toast.LENGTH_SHORT).show()
                    SharedPref.saveDestroyPass(this, editText.text.toString())
                }
            }
            builder.show()
        }

        lockImage.setOnClickListener {
            if (SharedPref.getIsUnlockPassSet(this) == "true") {
                val ft = supportFragmentManager.beginTransaction()
                button_start_remote.visibility = View.INVISIBLE
                ft.add(R.id.profilPageFragment, LockFragment())
                ft.commit()
            } else {
                Toast.makeText(this, "Please set the Unlock Password first ! !", Toast.LENGTH_SHORT).show()
            }
//            LockFragment.lock(this)
//
//            val ft = supportFragmentManager.beginTransaction()
//            button_start_remote.visibility = View.INVISIBLE
//            ft.add(R.id.profilPageFragment, LockFragment())
//            ft.commit()
//            Toast.makeText(this, "COMING SOON !", Toast.LENGTH_SHORT).show()
        }

        // finger print switch
        switchFP.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                if (SharedPref.getIsUnlockPassSet(this) == "false")
                {
                    Toast.makeText(applicationContext, "Please Unable Unlock Password", Toast.LENGTH_SHORT).show()
                    switchFP.isChecked = false
                }
                else
                    SharedPref.setFingerPrint("YES", this)
            } else {
                SharedPref.setFingerPrint("NO", this)
                // The switch is disabled
            }
        }

        switchUnlockPassword.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!isChecked)
            {
                SharedPref.setIsUnlockPassSet(this, "false")
                Toast.makeText(applicationContext, "Your pass not active !", Toast.LENGTH_SHORT).show()
                switchFP.isChecked = false
                switchDestroyPassword.isChecked = false
            }
            else
            {
                if (SharedPref.getUnlockPass(this) == "")
                {
                    switchUnlockPassword.isChecked = false
                    Toast.makeText(applicationContext, "Please set the Unlock Password first ! !", Toast.LENGTH_SHORT).show()
                }
                else
                {
                    SharedPref.setIsUnlockPassSet(this, "true")
                    Toast.makeText(applicationContext, "Your pass is now active !", Toast.LENGTH_SHORT).show()
                }
            }
        }

        switchDestroyPassword.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!isChecked)
            {
                SharedPref.setIsDestroyPassSet(this, "false")
                Toast.makeText(applicationContext, "Your Dpass is not active !", Toast.LENGTH_SHORT).show()
            }
            else
            {
                if (SharedPref.getIsUnlockPassSet(this) == "false")
                {
                    Toast.makeText(applicationContext, "Please Unable Unlock Password", Toast.LENGTH_SHORT).show()
                    switchDestroyPassword.isChecked = false
                }
                else if (SharedPref.getDestroyPass(this) == "")
                {
                    Toast.makeText(applicationContext, "Please set your Destroy Password", Toast.LENGTH_SHORT).show()
                    switchDestroyPassword.isChecked = false
                }
                else
                {
                    SharedPref.setIsDestroyPassSet(this, "true")
                    Toast.makeText(applicationContext, "Your pass is now active !", Toast.LENGTH_SHORT).show()
                }
            }
        }
        //            else
        //            {
        //                if (SharedPref.getIsUnlockPassSet(this) == false)
        //                {
        //                    Toast.makeText(applicationContext, "Please Unable Unlock Password", Toast.LENGTH_SHORT).show()
        //                    destroyPassicon.setImageResource(R.drawable.clear_black)
        //                }
        //                else if (SharedPref.getDestroyPass(this) == "")
        //                {
        //                    Toast.makeText(applicationContext, "Please set your Destroy Password", Toast.LENGTH_SHORT).show()
        //                    destroyPassicon.setImageResource(R.drawable.clear_black)
        //                }
        //                else {
        //                    SharedPref.setIsDestroyPassSet(this, true)
        //                    Toast.makeText(applicationContext, "Your pass is now active !", Toast.LENGTH_SHORT).show()
        //                    destroyPassicon.setImageResource(R.drawable.success2)
        //                }
        //            }



        copy.setOnClickListener {
            Toast.makeText(this, "Copied !", Toast.LENGTH_SHORT).show()
            val clipboard = this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("message", User.profil.name)
            clipboard.primaryClip = clip
        }

        profil_picture.setOnClickListener{
            val intent = Intent(this, remoteTuto::class.java)
            startActivity(intent)
        }

        button_start_remote.setOnClickListener {
            val intent = Intent(this, remoteDestroyFragment::class.java)
            startActivity(intent)
        }
    }
//
//    fun saveImage(myBitmap: Bitmap?): String {
//        val bytes = ByteArrayOutputStream()
//        myBitmap!!.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
//        val wallpaperDirectory = File(
//                Environment.getExternalStorageDirectory().toString() + IMAGE_DIRECTORY)
//        // have the object build the directory structure, if needed.
//
//        if (!wallpaperDirectory.exists()) {
//            Log.d("dirrrrrr", "" + wallpaperDirectory.mkdirs())
//            wallpaperDirectory.mkdirs()
//        }
//
//        try {
//            val f = File(wallpaperDirectory, Calendar.getInstance()
//                    .timeInMillis.toString() + ".jpg")
//            f.createNewFile()   //give read write permission
//            val fo = FileOutputStream(f)
//            fo.write(bytes.toByteArray())
//            MediaScannerConnection.scanFile(this,
//                    arrayOf(f.path),
//                    arrayOf("image/jpeg"), null)
//            fo.close()
//            Log.d("TAG", "File Saved::--->" + f.absolutePath)
//
//            return f.absolutePath
//        } catch (e1: IOException) {
//            e1.printStackTrace()
//        }
//
//        return ""
//
//    }
//
//    @Throws(WriterException::class)
//    fun TextToImageEncode(Value: String): Bitmap? {
//        val bitMatrix: BitMatrix
//        try {
//            bitMatrix = MultiFormatWriter().encode(
//                    Value,
//                    BarcodeFormat.QR_CODE,
//                    QRcodeWidth, QRcodeWidth, null
//            )
//
//        } catch (Illegalargumentexception: IllegalArgumentException) {
//
//            return null
//        }
//
//        val bitMatrixWidth = bitMatrix.getWidth()
//
//        val bitMatrixHeight = bitMatrix.getHeight()
//
//        val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)
//
//        for (y in 0 until bitMatrixHeight) {
//            val offset = y * bitMatrixWidth
//
//            for (x in 0 until bitMatrixWidth) {
//
//                pixels[offset + x] = if (bitMatrix.get(x, y))
//                    resources.getColor(R.color.purple)
//                else
//                    resources.getColor(R.color.white)
//            }
//        }
//        val bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)
//
//        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight)
//        return bitmap
//    }
}
