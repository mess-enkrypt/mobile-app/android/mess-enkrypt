package messnco.mess_enkrypt.viewControler

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_grouplist.*
import kotlinx.android.synthetic.main.fragment_bottom_grouplist_bar.*
import kotlinx.android.synthetic.main.fragment_navbar.*
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import messnco.mess_enkrypt.Listener.OnSwipeTouchListener
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.UI.GroupsAdapter
import messnco.mess_enkrypt.UI.UsersAdapter
import messnco.mess_enkrypt.models.*
import kotlin.system.exitProcess

@ImplicitReflectionSerializer
class groupListShareControlleur : AppCompatActivity() {

    private lateinit var listView : ListView
    private lateinit var groupFragm : Fragment
    var groupsInstance = Group.groupList
    lateinit var grpAdapter : GroupsAdapter


    companion object {
        lateinit var GrpContext : Context
        lateinit var listGrpView : ListView
        lateinit var sharedText : String
        val grpLC: groupListShareControlleur = groupListShareControlleur()
        lateinit var gA: GroupsAdapter
        var killApp = false

    }


    @ImplicitReflectionSerializer
    @UnstableDefault
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_grouplist)

        when {
            intent?.action == Intent.ACTION_SEND -> {
                if ("text/plain" == intent.type) {
                    handleSendText(intent) // Handle text being sent
                }
            }
            else -> {
                // Handle other intents, such as being started from the home screen
            }
        }
        val prefs = this.getSharedPreferences(Constants.profilFileName, 0)

        User.profil = Json.parse(User.serializer(), prefs.getString(Constants.profilID, "")!!)

        val userPref = this.getSharedPreferences(Constants.userListFileName, 0)
        if (userPref.getString(Constants.userList, "") == "{}")
            User.contacts = ArrayList<User>()
        else
            User.contacts = User.getUsers(userPref.getString(Constants.userList, "")!!)

        val adapter = UsersAdapter(this, User.contacts)
        userControlleur.uA = adapter

        val groupPref = this.getSharedPreferences(Constants.groupListFileName, 0)
        if (groupPref.getString(Constants.groupList, "") == "{}")
            Group.groupList = ArrayList<Group>()
        else
            Group.groupList = Group.getGroups(groupPref.getString(Constants.groupList, "")!!)
        CommunicationManager.comManager.launchReceiver(this, User.profil.name)

        groupsInstance = Group.groupList
        Toast.makeText(this, Group.groupList.first().groupName + Group.groupList.size, Toast.LENGTH_SHORT).show()
//        if (firstConnection.firstCo == true)
//        {
//            Log.d("Rtest", "this is the first co")
//            firstConnection.firstCo = false
//            val i2 = Intent(this, remoteTuto::class.java)
//            startActivity(i2)
//        }

        listView = this.findViewById(R.id.groupListShareView)
        listGrpView = listView
        GrpContext = this

        listView.setOnItemClickListener { _, _, position: Int, _: Long ->
            Toast.makeText(this, position.toString(), Toast.LENGTH_SHORT).show()
            val intent = Intent(this, groupListControlleur::class.java)
            intent.putExtra("pos", position)
            intent.putExtra("text", sharedText)
            startActivity(intent)

        }


        groupsInstance.forEach {
            Log.d("COO", "Grp : ${it.groupName} is active ? ${it.isActiv}")
        }
        //Log.d("COO", groupsInstance.toString())
        groupsInstance.sortByDescending { selector(it)}
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            groupsInstance.removeIf {
                !it.isActiv
            }
        }

        Log.d("COO", "J'ai fini de sortby")
        Log.d("COO", groupsInstance.toString())
        grpAdapter = GroupsAdapter(this, groupsInstance)

        updateList(grpAdapter)

        gA = grpAdapter

    }

    @ImplicitReflectionSerializer
    private fun handleSendText(intent: Intent) {
        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            sharedText = it
            // Update UI to reflect text being shared
        }
    }

    fun selector(g: Group): Long{
        Log.d("COO", g.messages.toString())
        if (g.messages.size > 1) {
            val lol = g.messages.last()
            return lol.createAt
        } else {
            return 3L
        }
        //return lol
    }

//    fun shareMsg(text: String) {
//        val sendIntent: Intent = Intent().apply {
//            action = Intent.ACTION_SEND
//            putExtra(Intent.EXTRA_TEXT, text)
//            type = "text/plain"
//        }
//
//        val shareIntent = Intent.createChooser(sendIntent, null)
//        startActivity(shareIntent)
//    }


    @UnstableDefault
    fun updateList(adapter : GroupsAdapter) {
        listView.adapter = adapter
    }

}