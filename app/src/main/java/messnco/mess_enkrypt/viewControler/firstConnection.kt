@file:Suppress("Annotator")

package messnco.mess_enkrypt.viewControler

//import moe.tlaster.kotlinpgp.KotlinPGP
//import moe.tlaster.kotlinpgp.data.GenerateKeyData

import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.View
import com.github.thibseisel.kdenticon.Identicon
import com.github.thibseisel.kdenticon.android.drawToBitmap

import io.socket.client.IO
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_first_connection.*
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.UnstableDefault
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.UI.UsersAdapter
import messnco.mess_enkrypt.models.CommunicationManager.Companion.comManager
import messnco.mess_enkrypt.models.SharedPref
import messnco.mess_enkrypt.models.User
import net.kibotu.pgp.Pgp
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream


class firstConnection : AppCompatActivity() {

    companion object{
        var firstCo = false

        fun BitMapToString(bitmap: Bitmap): String {
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 10, baos)
            val b = baos.toByteArray()
            return Base64.encodeToString(b, Base64.DEFAULT)
        }
    }

    @ImplicitReflectionSerializer
    @UnstableDefault
    @RequiresApi(Build.VERSION_CODES.GINGERBREAD)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_first_connection)


        connection.setOnClickListener {
            val uI = usernameInput.text.toString()
            //val pI = passwordInput.text.toString()
            firstCo = true
            if (uI.isNotEmpty()) {
                progressBar.visibility = View.VISIBLE
                val krgen = Pgp.generateKeyRingGenerator("".toCharArray())
                val publicKey = Pgp.genPGPPublicKey(krgen)
                val privateKey = Pgp.genPGPPrivKey(krgen)
                SharedPref.saveKeys(this, publicKey, privateKey)

                val socket = IO.socket("https://api.beta.socket.messenkrypt.com/")

                comManager.h.connect()
                        .on(Socket.EVENT_CONNECT, { Log.d("COO", "OUIIIIIIIIIIIIIIIIIII") })
                        .on("user:askauth") {
                            fun call (it: Array<Any>) {
                                runOnUiThread(Runnable () {
                                    fun run () {
                                        Log.d("COO", "user:askauth")
                                        if (User.profil.name.isEmpty()) {
                                            Log.d("COO", "user:askauth after if")
                                            Log.d("COO", publicKey)
                                            comManager.h.emit("user:register", usernameInput.text.toString(), publicKey)
                                                    .on("user:register") {
                                                        Log.d("COO", "user:register")
                                                        val receiveMSG = JSONArray(it)
                                                        val userToRecord = JSONObject(receiveMSG[0].toString())

                                                        // Create a new instance of the Identicon class with an hash string and the given size
                                                        // Create a new instance of the Identicon class with an hash string and the given size
                                                        val iconSize = 300
                                                        val icon = Identicon.fromValue(userToRecord["username"].toString(), iconSize)
                                                        val bitmap = Bitmap.createBitmap(300, 300, Bitmap.Config.ARGB_8888)
                                                        icon.drawToBitmap(bitmap)
                                                        Log.d("COO", BitMapToString(bitmap))


                                                        // Creates a new file with the given name
                                                        // Creates a new file with the given name
//                                                        icon.saveAsSvg("kdenticon.svg" as OutputStream)

                                                        User.profil = User(userToRecord["username"].toString(),0,BitMapToString(bitmap),userToRecord["publicKey"].toString(),5,userToRecord["destroyPassphrase"].toString())
                                                        SharedPref.saveUser(this, "")
                                                        SharedPref.setIsUnlockPassSet(this, "false")
                                                        comManager.h.emit("user:login", User.profil.name)
                                                        //onRegister(it, socket, pI)
                                                    }
                                        }
                                    }
                                    run()
                                })
                            }
                            call(it)

                /*                            socket.on("user:register", {
                                                onRegister(it, socket, pI)
                                            })

                 */
                        }
                        .on("user:login", Emitter.Listener(){
                            fun call (it: Array<Any>) {
                                runOnUiThread(Runnable () {
                                    fun run () {
                                        onLogin(it, socket)
                                    }
                                    run()
                                })
                            }
                            call(it)
                        })
                        .on("user:isDestroyed", Emitter.Listener(){
                            Log.d("COO", "ASKIP JE SUIS EN user:login:err")
                            groupListControlleur.toDestroy(this)
                        })
                        .on("user:login:err", Emitter.Listener(){

                        })

            }

        }
    }

    @ImplicitReflectionSerializer
    fun onAskAuth(it: Array<Any>, publicKey: String, socket: Socket) {

        Log.d("COO", "user:askauth")
        if (User.profil.name.isEmpty()) {
            comManager.h.emit("user:register", usernameInput.text.toString(), publicKey)
        }
    }

    @ImplicitReflectionSerializer
    @UnstableDefault
    fun onRegister(it: Array<Any>, socket: Socket, pI: String) {
        Log.d("COO", "user:register")
        val receiveMSG = JSONArray(it)
        val userToRecord = JSONObject(receiveMSG[0].toString())
        User.profil = User(userToRecord["username"].toString(),0,"",userToRecord["publicKey"].toString(),5,userToRecord["destroyPassphrase"].toString())
        SharedPref.saveUser(this, pI)
        comManager.h.emit("user:login", User.profil.name)
    }

    @ImplicitReflectionSerializer
    fun onLogin(it: Array<Any>, socket: Socket) {
        val receiveMSG = JSONArray(it)
        val hihi = JSONObject(receiveMSG[0].toString())
        Log.d("COO", hihi.toString())
        comManager.h.emit("user:auth", "lol", User.profil.name)
        comManager.receiveSocket(this, socket)
        val adapter = UsersAdapter(this, ArrayList<User>())
        userControlleur.uA = adapter
        val i2 = Intent(this, groupListControlleur::class.java)
        i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        i2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(i2)
    }

}
