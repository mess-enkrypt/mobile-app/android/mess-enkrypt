package messnco.mess_enkrypt.viewControler

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.fragment_add_user_friend.*
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.json.JSON
import kotlinx.serialization.json.Json
import kotlinx.serialization.stringify
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.models.CommunicationManager.Companion.comManager
import messnco.mess_enkrypt.models.Constants
import messnco.mess_enkrypt.models.SharedPref
import messnco.mess_enkrypt.models.User
import org.json.JSONArray
import org.json.JSONObject

class addContact : AppCompatActivity() {
    @ImplicitReflectionSerializer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_add_user_friend)

        groupListControlleur.GrpContext = this

        back_profile.setOnClickListener {
//            onBackPressed()
            finish()
        }


        button_add.setOnClickListener {

            if (addFriendText.text.toString() != User.profil.name) {

                Log.d("COO", "Before Emitting")
                Log.d("COO", addFriendText.text.toString())
                comManager.h.emit("user:getPublicKey", addFriendText.text.toString())
                finish()
//                val intent = Intent(this, userControlleur::class.java)
//                startActivity(intent)
//                        .on("user:getPublicKey:success", {
//                             fun call (it: Array<Any>) {
//                                runOnUiThread(Runnable () {
//                                    fun run () {
//                                        onSuccess(it)
//                                    }
//                                    run()
//                                })
//                             }
//                            call(it)
//                        })
//                        .on("user:getPublicKey:err", {
//                            fun call (it: Array<Any>) {
//                                runOnUiThread(Runnable () {
//                                    fun run () {
//                                        onError(it)
//                                    }
//                                    run()
//                                })
//                            }
//                            call(it)
//                        })
            }
            else{
                textViewAddContact.text = "It's you"
                return@setOnClickListener
            }
        }

    }

    @ImplicitReflectionSerializer
    fun onSuccess(it: Array<Any>) {
        Log.d("COO", "on receiving GetPublicKey")
        val receiveMSG = JSONArray(it)
        val JSONdata = JSONObject(receiveMSG[0].toString())
        val strToSave = JSONdata.getString("publicKey")
        Log.d("COO", strToSave)
        val newUser = User(addFriendText.text.toString(), 1, "", strToSave, 0, "")
        var isFind = "false"
//          log de test pour savoir combien de fois cette fonction est lancé quand on appuie sur addcontact
//        Log.d("test", "LOG TEST")
        for (x in 0 until User.contacts.size){
            //Log.d("test", "check : " + User.contacts[x].name + " and " + newUser.name)
            if (newUser.name == User.contacts[x].name && newUser.publicKey == User.contacts[x].publicKey) {
                Toast.makeText(this, "User Already Exist",
                       Toast.LENGTH_SHORT).show()
                Log.d("COO", "User Already Exist")
                isFind = "true"
            }
        }
        if (isFind == "false") {
            User.contacts.add(newUser)
            SharedPref.saveContacts(this)

            finish()
//            val intent = Intent(this, userControlleur::class.java)
//            startActivity(intent)
        }
    }

    fun onError(it: Array<Any>) {
        val receiveMSG = JSONArray(it)
        val strToSave = receiveMSG[0].toString()
        if (strToSave == "USER_NOT_ACTIVE")
        {
            Log.d("COO", "User Not Active")
            Toast.makeText(this, "User Not Active",
                    Toast.LENGTH_SHORT).show()
        } else if (strToSave == "USER_NOT_FOUND") {
            Log.d("COO", "User Not Found")
            Toast.makeText(this, "User Not Found",
                    Toast.LENGTH_SHORT).show()
        }
    }

}