package messnco.mess_enkrypt.viewControler

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.fragment_navbar.*
import kotlinx.serialization.ImplicitReflectionSerializer
import messnco.mess_enkrypt.R

@ImplicitReflectionSerializer
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val GroupC = Intent(this, groupListControlleur::class.java)
        startActivity(GroupC)


        contactButton.setOnClickListener {
            try {
                val contactB = Intent(this, userControlleur::class.java)
                startActivity(contactB)
            } catch (e: Exception) {
                println(e)
            }
        }

        settingsButton.setOnClickListener{
            val intent = Intent(this, profilPage::class.java)
            startActivity(intent)
        }

        settings.setOnClickListener{
            val intent = Intent(this, profilPage::class.java)
            startActivity(intent)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
