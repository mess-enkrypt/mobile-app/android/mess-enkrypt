package messnco.mess_enkrypt.viewControler

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.lock_fragment.view.*
import messnco.mess_enkrypt.R
import net.kibotu.pgp.Pgp
import org.json.JSONObject
import kotlin.random.Random
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import kotlinx.serialization.ImplicitReflectionSerializer
import messnco.mess_enkrypt.models.*
import messnco.mess_enkrypt.models.CommunicationManager.Companion.comManager
import org.json.JSONArray

/* LOCK
1. Generate Random String (like in firstConnection)
2. Encrypt JSON files from assets with random string from 1
3. Encrypt Random String  with publicKey
4. POST the result from step 3 to /encrypt routes
5. Save the result from step 4
 */

/* UNLOCK
1. Get the result from step 5 from LOCK
2. POST result of step 1 to /decrypt route
3. Decrypt result of step 2 with privateKey
4. Decrypt JSON files with result of step 3
 */


@ImplicitReflectionSerializer
class LockFragment : Fragment(){

    @ImplicitReflectionSerializer
    companion object {
        fun newInstance() = LockFragment()

        private var PRIVATE_MODE = 0

        fun lock(cContext: Context){
            //val params = HashMap<String,String>()

            // ADD CRYPT JSON

            //val url = Constants.base_api + "/encrypt"

            val randomString = (1..8192)
                    .map { i -> Random.nextInt(0, Constants.charPool.size) }
                    .map(Constants.charPool::get)
                    .joinToString()
            val profilPrefs = cContext.getSharedPreferences(Constants.profilFileName, 0)
            val pass = profilPrefs.getString(Constants.pass, "")

            if (Group.groupList.isNotEmpty()) {
                val toSend = MCrypto.encryptAndSavePasswordLock(cContext, pass)
                comManager.h.emit("user:lock", toSend)
                        .on("user:lock") {
                            val prefs = cContext.getSharedPreferences(Constants.profilFileName, PRIVATE_MODE)
                            //val isLockPref = prefs.getString(Constants.isLock, "")
                            val editorLock = prefs.edit()
                            editorLock.putString(Constants.isLock, "true")
                            editorLock.apply()
                /*                            val receiveMSG = JSONArray(it)
                                            val strToSave = receiveMSG[0].toString()


                                            val prefs = cContext.getSharedPreferences(Constants.profilFileName, PRIVATE_MODE)
                                            //val isLockPref = prefs.getString(Constants.isLock, "")
                                            val editorLock = prefs.edit()
                                            editorLock.putString(Constants.isLock, "true")
                                            editorLock.apply()

                                            val encryptPrefs = cContext.getSharedPreferences(Constants.encryptedLock, PRIVATE_MODE)
                                            val editor = encryptPrefs.edit()
                                            editor.putString(Constants.el, strToSave)
                                            editor.apply()

                 */
                            Log.d("COO", "Je reçois un user:lock")
                        }

            }
        }

        fun unlock(cContext: Context) {
            Log.d("Log ", "Unlock function")

            val encryptPrefsUnlock = cContext.getSharedPreferences(Constants.encryptedLock, PRIVATE_MODE)
            val en = encryptPrefsUnlock.getString(Constants.el, "")
            comManager.h.emit("user:unlock", en)
                    .on("user:unlock") {
                        val receiveMSG = JSONArray(it)
                        val strToSave = receiveMSG[0].toString()
                        //val strToSave = jsonReceiveMSG["decrypted"].toString()
                        Log.d("COO", "unlock")
                        Log.d("COO", strToSave)

                        //val keysPrefs = cContext.getSharedPreferences(Constants.keysFileName, 0)

                        //val privateKey = keysPrefs.getString(Constants.privateKey, "")

                        //Pgp.setPrivateKey(privateKey)
            /*
                                    val userTab = User.profil.name.split("#")
                                    val profilPrefs = cContext.getSharedPreferences(Constants.profilFileName, 0)
                                    val pass = profilPrefs.getString(Constants.pass, "")
                                    val decr = Pgp.decrypt(strToSave, pass)
                                    MCrypto.getDecryptedPasswordUnlock(cContext, decr)


             */
                        //Log.d("COO", decr)
                        val prefs = cContext.getSharedPreferences(Constants.profilFileName, PRIVATE_MODE)
                        //val isLockPref = prefs.getString(Constants.isLock, "")
                        val editorLock = prefs.edit()
                        editorLock.putString(Constants.isLock, "false")
                        editorLock.apply()

                    }

        }
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.lock_fragment, container, false)

        groupListControlleur.GrpContext = context

        val prefs = context.getSharedPreferences(Constants.profilFileName, PRIVATE_MODE)
        //val isLockPref = prefs.getString(Constants.isLock, "")
        val editorLock = prefs.edit()
        editorLock.putString(Constants.isLock, "true")
        editorLock.apply()

        view.unlockButton.setOnClickListener{

                val profilPrefs = context.getSharedPreferences(Constants.profilFileName, 0)
                val pass = profilPrefs.getString(Constants.unlockPass, "")
                val builder = AlertDialog.Builder(context)
                val inflater = layoutInflater
                builder.setTitle("Enter your password")
                val dialogLayout = inflater.inflate(R.layout.alert_dialog_with_edittext, null)
                val editText  = dialogLayout.findViewById<EditText>(R.id.editText)
                builder.setView(dialogLayout)
                builder.setPositiveButton("OK") { dialogInterface, i ->
                    if (editText.text.toString() == pass) {
                        unlock(context)
                        val prefs = context.getSharedPreferences(Constants.profilFileName, PRIVATE_MODE)
                        //val isLockPref = prefs.getString(Constants.isLock, "")
                        val editorLock = prefs.edit()
                        editorLock.putString(Constants.isLock, "false")
                        editorLock.apply()
                        val intent = Intent(activity, groupListControlleur::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    } else {
                        Toast.makeText(context, "Sorry you wrote a wrong password !", Toast.LENGTH_SHORT).show()
                    }
                }
                builder.show()
        }

        return view
    }

}