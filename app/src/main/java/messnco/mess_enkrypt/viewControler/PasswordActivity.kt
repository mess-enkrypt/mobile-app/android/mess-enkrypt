package messnco.mess_enkrypt.viewControler

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_unlock_password.*
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.json.Json
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.models.*
import kotlin.system.exitProcess

@ImplicitReflectionSerializer
class PasswordActivity: AppCompatActivity() {

    override fun onRestart() {
        super.onRestart()
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unlock_password)

        groupListControlleur.GrpContext = this

        button_log.setOnClickListener {
            if (SharedPref.getDestroyPass(this) == editTextPass.text.toString())
            {
                SharedPref.clearSharedPref(this)
                val alertDialog = AlertDialog.Builder(this).create()
                alertDialog.setTitle("Data Destroy")
                val fulls = "It looks that your data are compromised.\n Your app is going to shutdown"
                alertDialog.setMessage(fulls)
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK"
                ) { dialog, which -> dialog.dismiss() }
                alertDialog.show()

                val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
                layoutParams.weight = 10f
                btnPositive.layoutParams = layoutParams


                btnPositive.setOnClickListener{
                    exitProcess(0)
                }

            }
            else if (SharedPref.getUnlockPass(this) == editTextPass.text.toString())
            {
                val prefs = this.getSharedPreferences(Constants.profilFileName, 0)

                User.profil = Json.parse(User.serializer(), prefs.getString(Constants.profilID, "")!!)

                val userPref = this.getSharedPreferences(Constants.userListFileName, 0)
                if (userPref.getString(Constants.userList, "") == "{}")
                    User.contacts = ArrayList<User>()
                else
                    User.contacts = User.getUsers(userPref.getString(Constants.userList, "")!!)

                val groupPref = this.getSharedPreferences(Constants.groupListFileName, 0)
                if (groupPref.getString(Constants.groupList, "") == "{}")
                    Group.groupList = ArrayList<Group>()
                else
                    Group.groupList = Group.getGroups(groupPref.getString(Constants.groupList, "")!!)
                CommunicationManager.comManager.launchReceiver(this, User.profil.name)

                val group = Intent(baseContext, groupListControlleur::class.java)
                // start your next activity
                SharedPref.setIntentFingerPrint(0, this)
                startActivity(group)
            }
            else
            {
                Toast.makeText(this, "Bad Passsword !", Toast.LENGTH_SHORT).show()
            }
        }
    }
}