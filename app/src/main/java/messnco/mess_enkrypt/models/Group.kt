package messnco.mess_enkrypt.models

import android.content.Context
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.Serializable
import messnco.mess_enkrypt.viewControler.groupCreate
import messnco.mess_enkrypt.viewControler.groupCreate.Companion.groupListToJson
import org.json.JSONException
import org.json.JSONObject

@Serializable
class Group(
        var groupName : String = "",
        var id : Int = 0,
        var groupPic : String = "",
        var timeDeletion : Long = 0,
        var members : ArrayList<User> = ArrayList<User>(),
        var admins : ArrayList<User> = ArrayList<User>(),
        var messages : ArrayList<MMessage> = ArrayList<MMessage>(),
        var isActiv : Boolean = true,
        var isFav : Boolean = false
) {

    companion object {

        var groupList = ArrayList<Group>()

        fun setGroupActiv(grp: Int, isIt : Boolean) {
            groupList[grp].isActiv = isIt
        }

        fun deleteGroup(grp: Group) {
            groupList.remove(grp)
        }

        fun getGroups(JSONList : String): ArrayList<Group> {
            val groupsList = ArrayList<Group>()

            try {
                // Load data
                val json = JSONObject(JSONList)
                val groups = json.getJSONArray("groups")

                // Get Recipe objects from data
                (0 until groups.length()).mapTo(groupsList) {
                    Group(groups.getJSONObject(it).getString("groupName"),
                            groups.getJSONObject(it).getInt("id"),
                            groups.getJSONObject(it).getString("groupPic"),
                            groups.getJSONObject(it).getLong("timeDeletion"),
                            User.getUsers(groups.getJSONObject(it).getString("members")),
                            User.getUsers(groups.getJSONObject(it).getString("admins")),
                            MMessage.getMessages(groups.getJSONObject(it).getString("messages")),
                            groups.getJSONObject(it).getBoolean("isActiv"),
                            groups.getJSONObject(it).getBoolean("isFav"))
//                            Boolean(groups.getJSONObject(it).getString("isActiv")))
//                            groups.getJSONObject(it).getBoolean(groups.getJSONObject(it).getString("isActiv")))

                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return groupsList
        }

        fun isMessageIdIsNew(g: Group, mid: Int) : Boolean {
            g.messages.forEach {
                if (it.ID ==  mid) {
                    return false
                }
            }
            return true
        }

        private fun loadJsonFromAsset(filename: String, context: Context): String? {
            val json: String?

            try {
                val inputStream = context.assets.open(filename)
                val size = inputStream.available()
                val buffer = ByteArray(size)
                inputStream.read(buffer)
                inputStream.close()
                json = String(buffer, Charsets.UTF_8)
            } catch (ex: java.io.IOException) {
                ex.printStackTrace()
                return null
            }

            return json
        }
    }
}