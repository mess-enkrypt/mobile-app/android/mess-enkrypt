package messnco.mess_enkrypt.models

import io.socket.client.Socket

class Constants {
// TODO fill up the constant class with
    //file pref names
    //constant number
    //find the rest

    companion object {
        const val username: String = "profil_username"
        const val profilFileName: String = "messencrypt.profil.prefs"
        const val isLock: String = "isLock_store"
        const val pass: String = "pass_store"
        const val profilID: String = "profil_store"
        const val publicKey: String = "publicKey_store"
        const val unlockPass: String = "unlockPass_store"
        const val destroyPass: String = "destroyPass_store"
        const val isUnlockPassSet: String = "isUnlockPassSet_store"
        const val isDestroyPassSet: String = "isDestroyPassSet_store"

        const val keysFileName: String = "messenkrypt.keys.prefs"
        const val privateKey: String = "privateKey_store"

        const val encryptFileName: String = "messenkrypt.keys.prefs"
        const val encryptString: String = "encryptString_store"

        const val userListFileName: String = "messencrypt.userList.prefs"
        const val userList: String = "userList_store"

        const val groupListFileName: String = "messencrypt.groupList.prefs"
        const val groupList: String = "groupList_store"

        const val settingsFileName: String = "messencrypt.settings.prefs"
        const val settings: String = "settings_store"

        const val encryptedLock: String = "messencrypt.lock.prefs"
        const val el: String = "encrypted_password"

        const val prefsFileName: String = "messenkrypt.prefs.prefs"
        const val useFingerPrint: String = "use_fingerprint"
        const val fingerPrintItent: String = "intent_fingerPrint"


        val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

        const val base_api_dev: String = "https://api.dev.messenkrypt.xyz"
        const val base_api_beta: String = "https://api.beta.socket.messenkrypt.com"
        const val base_api_prod: String = "https://api.messenkrypt.xyz"
        const val base_api: String = base_api_beta

    }
}