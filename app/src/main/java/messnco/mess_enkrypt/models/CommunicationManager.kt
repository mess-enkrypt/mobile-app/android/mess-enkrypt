package messnco.mess_enkrypt.models

import android.content.Context
import android.util.Log
import io.socket.client.IO
import io.socket.client.Socket
import kotlinx.serialization.ImplicitReflectionSerializer
import org.json.JSONArray
import org.json.JSONObject
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.ZoneId
import android.os.*
import android.support.annotation.UiThread
import io.socket.emitter.Emitter
import messnco.mess_enkrypt.viewControler.*
import net.kibotu.pgp.Pgp
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.widget.Toast
import com.github.thibseisel.kdenticon.Identicon
import com.github.thibseisel.kdenticon.android.drawToBitmap
import kotlinx.serialization.UnstableDefault
import messnco.mess_enkrypt.viewControler.groupListControlleur.Companion.grpLC
import messnco.mess_enkrypt.viewControler.groupListControlleur.Companion.listGrpView
import messnco.mess_enkrypt.viewControler.groupMainFragment.Companion.mA


@ImplicitReflectionSerializer
class CommunicationManager() {


    companion object {
        val comManager: CommunicationManager = CommunicationManager()

    }

    var h = IO.socket("https://api.dev.messenkrypt.xyz/")
    private data class addContactQ(val n : String, val id : Int){
        var name : String = n
        var gid : Int = id
    }

    private var contactQ = ArrayList<addContactQ>()

    @UiThread
    @ImplicitReflectionSerializer
    fun receiveSocket(context: Context, socket: Socket) {

        //h = socket as Socket
        h.on("message:receive") {
            fun call (it: Array<Any>) {
                grpLC.runOnUiThread(Runnable () {
                    fun run () {
                        Log.d("COO", "Je suis dans le receive socket")
                        val receiveMSG = JSONArray(it)
                        val jsonReceiveMSG = JSONObject(receiveMSG[0].toString())
                        val from = jsonReceiveMSG["from"].toString()
                        Log.d("Rtest", "Message Reçu de " + from)
                        val to = jsonReceiveMSG["to"].toString()
                        if (from != User.profil.name && to == User.profil.name) {

                            val keysPrefs = context.getSharedPreferences(Constants.keysFileName, 0)
                            val profilPrefs = context.getSharedPreferences(Constants.profilFileName, 0)
                            val pass = profilPrefs.getString(Constants.pass, "")
                            val privateKey = keysPrefs.getString(Constants.privateKey, "")

                            Pgp.setPrivateKey(privateKey!!)
                            val decrypt = Pgp.decrypt(jsonReceiveMSG["msg"].toString(), "")
                            val jsonDataMSG = JSONObject(decrypt)
                            Log.d("Rtest", "le type du message est : " + jsonDataMSG["type"].toString())
                            if (jsonDataMSG["type"].toString() == "1") {
                                getNewGroup(jsonDataMSG, context, jsonReceiveMSG["from"].toString())
                            } else {
                                getMessage(jsonDataMSG, jsonReceiveMSG.getString("date"), jsonReceiveMSG["from"].toString(), context)
                                SharedPref.saveGroups(context)
                                Log.d("COO", groupMainFragment.isInit.toString())
                                if (groupMainFragment.isInit > 0) {
                                    //val target = getGroupInListById(jsonDataMSG.getInt("grpId"))
                                    groupMainFragment.test(context, jsonDataMSG.getInt("grpId"))
                                    Log.d("COO", "Before Changed")
                                    //mA.notifyDataSetChanged()
                                }
                                //groupMainFragment.refreshList(context, jsonDataMSG.getInt("grpId"))
//                                groupListControlleur.gA.notifyDataSetChanged()
                            }

                        }
                    }
                    run()

                })
            }
            call(it)
            Log.d("COO", "message:receive")

        }
        .on("message:send:err") {
            val receiveMSG = JSONArray(it)
            if (receiveMSG[0].toString() == "USER_IS_LOCKED") {
                Log.d("COO", "USER_IS_LOCKED")
            }
        }
        .on("user:getPublicKey:success") {
            Log.d("COO", "on receiving GetPublicKey:success")
            val receiveMSG = JSONArray(it)
            val JSONdata = JSONObject(receiveMSG[0].toString())
            val strToSave = JSONdata.getString("publicKey")
            val nameToSave = JSONdata.getString("username")
            Log.d("COO", strToSave)
            val iconSize = 300
            val icon = Identicon.fromValue(nameToSave, iconSize)
            val bitmap = Bitmap.createBitmap(300, 300, Bitmap.Config.ARGB_8888)
            icon.drawToBitmap(bitmap)
            Log.d("COO", firstConnection.BitMapToString(bitmap))
            val newUser = User(nameToSave, 1, firstConnection.BitMapToString(bitmap), strToSave, 0, "")
            val isT = getUserInList(nameToSave)
            if (isT.name == "no") {
                User.contacts.add(newUser)
                if (!contactQ.isEmpty())
                {
                    var tmpInt = -1
                    for (i in 0 until contactQ.size)
                    {
                        if (contactQ[i].name == newUser.name)
                        {
                            val tmpGrp = getGroupInListById(contactQ[i].gid)
                            Group.groupList[Group.groupList.indexOf(tmpGrp)].members.add(newUser)
                            tmpInt = i
                        }
                    }
                    if (tmpInt >= 0)
                        contactQ.removeAt(tmpInt)
                }
            }
            fun call (it: Array<Any>) {
                grpLC.runOnUiThread(Runnable() {
                    fun run() {
                        userControlleur.uA.notifyDataSetChanged()
                    }
                    run()
                })
            }
            call(it)
            SharedPref.saveContacts(context)
        }
                .on("user:getPublicKey:err") {
                    Log.d("COO", "on receiving GetPublicKey:err")
                    fun call (it: Array<Any>) {
                        grpLC.runOnUiThread(Runnable() {
                            fun run() {
                                val receiveMSG = JSONArray(it)
                                val strToSave = receiveMSG[0].toString()
                                if (strToSave == "USER_NOT_ACTIVE")
                                {
                                    Log.d("COO", strToSave)
                                    Toast.makeText(context, strToSave, Toast.LENGTH_SHORT).show()
                                } else if (strToSave == "USER_NOT_FOUND") {
                                    Log.d("COO", strToSave)
                                    Toast.makeText(context, strToSave, Toast.LENGTH_SHORT).show()
                                } else {
                                    Toast.makeText(context, "User Not Found", Toast.LENGTH_LONG).show()

                                }
                            }
                            run()
                        })
                    }
                    call(it)
                }
    }

    @UnstableDefault
    @ImplicitReflectionSerializer
    fun launchReceiver(context: Context, username: String) {


        h.connect()
                .on(Socket.EVENT_CONNECT, { Log.d("COO", "OUIIIIIIIIIIIIIIIIIII") })
                .on("user:askauth", Emitter.Listener() {
                    Log.d("COO", "On AskAuth")
                    Log.d("COO", User.profil.name)
                    h.emit("user:login", User.profil.name)
                })
                .on("user:login", Emitter.Listener() {
                    Log.d("COO", "Je suis dans le user:login")
                    h.emit("user:auth", "lol", User.profil.name)
                    receiveSocket(context, h)
                    MNotification.initNotif(context)
                })
                .on("user:login:err", Emitter.Listener() {
                    Log.d("COO", "On LoginErr")
                    val receiveMSG = JSONArray(it)
                    if (receiveMSG[0].toString() == "USER_NOT_ACTIVE") {
                        Log.d("COO", "ASKIP JE SUIS EN user:login:err")
                        groupListControlleur.toDestroy(context)
                    }
                })
                .on("user:isDestroyed", Emitter.Listener() {
                    Log.d("COO", "ASKIP JE SUIS EN user:login:err")
                    groupListControlleur.toDestroy(context)
                })
                .on("err", Emitter.Listener{
                    Log.d("COO", "||")

                })
                .on(Socket.EVENT_DISCONNECT, Emitter.Listener() {
                    Log.d("COO", "DISCONECTED")
                    val intent = Intent(context, loadScreen::class.java)
                    context.startActivity(intent)
//                    fun call (it: Array<Any>) {
//                        grpLC.runOnUiThread(Runnable() {
//                            fun run() {
//                                Toast.makeText(context, "You have been disconected from MessEnkrypt", Toast.LENGTH_SHORT).show()
//                                grpLC.finish()
//                                System.exit(2)
//                            }
//                            run()
//                        })
//                    }
//                    call(it)
                })
    }

    fun getMessage(obj: JSONObject, date : String, from: String, context: Context) {
        val newMsg = MMessage()
        Log.d("COO", obj.toString())
        val target = getGroupInListById(obj.getInt("grpId"))
        if (target.groupName == "") {
            Toast.makeText(context, "You received a message from a deleted group", Toast.LENGTH_SHORT).show()
        }
        if (target.groupName.isNotEmpty() && Group.isMessageIdIsNew(target, obj.getInt("id")) ) {
            if (obj.getString("picture") != "") {
                newMsg.picture = obj.getString("picture")
            }
            val keysPrefs = context.getSharedPreferences(Constants.keysFileName, 0)
            val privateKey = keysPrefs.getString(Constants.privateKey, "")
            Pgp.setPrivateKey(privateKey!!)
            val profilPrefs = context.getSharedPreferences(Constants.profilFileName, 0)
            val pass = profilPrefs.getString(Constants.pass, "")
            val decrypt = Pgp.decrypt(obj.getString("content"), pass!!)
            Log.d("COO", decrypt)
            newMsg.content = decrypt.toString()
            Log.d("Rtest", "le contenu d'un message de " +  getUserInList(from).name + " est : " + decrypt.toString())

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                newMsg.createAt = LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
            }
            newMsg.sender = getUserInList(from)
            newMsg.ID = Group.groupList[Group.groupList.indexOf(target)].messages.size
            newMsg.isMe = false
            Group.groupList[Group.groupList.indexOf(target)].messages.add(newMsg)
            if (MNotification.isInit == "no") {
                MNotification.initNotif(context)
            }
            MNotification.createNotifMessage(context, newMsg.content)
        }
    }

    @ImplicitReflectionSerializer
    fun getNewGroup(obj: JSONObject, context: Context, from: String) {
        val newGrp = Group()

        newGrp.groupName = obj.getString("grpName")
        newGrp.groupPic = obj.getString("groupPic")
//        newGrp.pubGroupKey = obj.getString("pubGroupKey")
//        newGrp.privGroupKey = obj.getString("privGroupKey")
        newGrp.id = obj.getInt("gid")
        newGrp.isActiv = true
        newGrp.isFav = false

        Log.d("Rtest", newGrp.groupName)

        val test = getGroupInListById(obj.getInt("gid"))
        if (test.groupName.isEmpty()) {
            val userList = JSONArray(obj.getString("userlist"))
            for (x in 0 until userList.length()) {
                val newUser = getUserInList(userList.get(x).toString())
                if (newUser.name == "no") {
                    if (userList.get(x).toString() != User.profil.name) {
                        comManager.h.emit("user:getPublicKey", userList.get(x).toString())
                        var addQ = addContactQ(userList.get(x).toString(), newGrp.id)
                        contactQ.add(addQ)

//                        addContactSocket(userList.get(x).toString(), context, newGrp)
                        //newGrp.members.add(newUser)
                    }
                } else
                    newGrp.members.add(newUser)
            }
            Log.d("Rtest", "Printing the user list of the new group :" + newGrp.groupName)
            for (x in 0 until newGrp.members.size)
            {
                Log.d("Rtest", newGrp.members[x].name)
            }
            Group.groupList.add(newGrp)
            Log.d("COO", newGrp.toString())
            Log.d("COO", "PUTAINNNNNNN")
            Log.d("COO", newGrp.toString())
            if (MNotification.isInit == "no") {
                MNotification.initNotif(context)
            }
            MNotification.createNotifGroupe(context, newGrp.groupName)
            SharedPref.saveGroups(context)
            groupListControlleur.gA.notifyDataSetChanged()
        }


    }

    private fun getUserInList(name: String): User {
        User.contacts.forEach {
            if (it.name == name)
                return it
        }
        return User("no")
    }

    fun getGroupInListById(id: Int): Group {
        Group.groupList.forEach{
            if (it.id == id)
                return it
        }
        return Group()
    }

}

//    private fun addContactSocket(username: String, context: Context, newGrp: Group) {
////        comManager.h.emit("user:getPublicKey", username)
//                comManager.h.on("user:getPublicKey:success", {
//                    Log.d("COO", "on receiving GetPublicKey")
//                    val receiveMSG = JSONArray(it)
//                    val JSONdata = JSONObject(receiveMSG[0].toString())
//                    val strToSave = JSONdata.getString("publicKey")
//                    Log.d("COO", strToSave)
//                    val newUser = User(username, 1, "", strToSave, 0, "")
//                    val isT = getUserInList(username)
//                    if (isT.name == "no") {
//                        User.contacts.add(newUser)
//                        SharedPref.saveContacts(context)
//                        newGrp.members.add(newUser)
//                    }
////                    var isThere = "no"
////                    for (x in 0 until User.contacts.size){
////                        if (newUser.name == User.contacts[x].name && newUser.publicKey == User.contacts[x].publicKey) {
////                            isThere = "yes"
////
////                        }
////                    }
////                    if (isThere == "no") {
////                        User.contacts.add(newUser)
////                        SharedPref.saveContacts(context)
////                    }
////                            }
////                            run()
////                        })
////                    }
////                    call(it)
//                })
//                .on("user:getPublicKey:err", {
//                    fun call (it: Array<Any>) {
//                        grpLC.runOnUiThread(Runnable() {
//                            fun run() {
//                                val receiveMSG = JSONArray(it)
//                                val strToSave = receiveMSG[0].toString()
//                                if (strToSave == "USER_NOT_ACTIVE")
//                                {
//                                    Log.d("COO", strToSave)
//                                    Toast.makeText(context, strToSave, Toast.LENGTH_SHORT).show()
//                                } else if (strToSave == "USER_NOT_FOUND") {
//                                    Log.d("COO", strToSave)
//                                    Toast.makeText(context, strToSave, Toast.LENGTH_SHORT).show()
//                                }
//                            }
//                            run()
//                        })
//                    }
//                    call(it)
//                })
//    }
