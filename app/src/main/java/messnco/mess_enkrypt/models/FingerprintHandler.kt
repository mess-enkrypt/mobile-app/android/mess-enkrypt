package messnco.mess_enkrypt.models

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.CancellationSignal
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.widget.Toast
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import messnco.mess_enkrypt.viewControler.PasswordActivity
import messnco.mess_enkrypt.viewControler.groupListControlleur

class FingerPrintHandler(private val appContext: Context) : FingerprintManager.AuthenticationCallback() {
    private var cancellationSignal: CancellationSignal? = null
    private var tryIntent : Int = SharedPref.getIntentFingerPrint(appContext)

    fun startAuth(manager: FingerprintManager, cryptoObject: FingerprintManager.CryptoObject) {
        cancellationSignal = CancellationSignal()
        if (ActivityCompat.checkSelfPermission(appContext, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null)
    }

    override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
        Toast.makeText(appContext, "Authentication error : $errString", Toast.LENGTH_SHORT).show()
    }

    override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence?) {
        Toast.makeText(appContext, "Authentication help : $helpString", Toast.LENGTH_SHORT).show()
    }

    @ImplicitReflectionSerializer
    override fun onAuthenticationFailed() {
        Toast.makeText(appContext, "Authentication Failed !", Toast.LENGTH_SHORT).show()
        tryIntent += 1
        SharedPref.setIntentFingerPrint(tryIntent, appContext)
        if (tryIntent > 4)
        {
            val prefs = appContext.getSharedPreferences(Constants.profilFileName, 0)
            if (SharedPref.getIsUnlockPassSet(appContext) == "true")
            {
                val p = Intent(appContext, PasswordActivity::class.java)
                appContext.startActivity(p)
            }
        }
    }

    @ImplicitReflectionSerializer
    override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult?) {
        Toast.makeText(appContext, "Authentication Succed", Toast.LENGTH_SHORT).show()
        SharedPref.setIntentFingerPrint(0, appContext)

        val prefs = appContext.getSharedPreferences(Constants.profilFileName, 0)

        User.profil = Json.parse(User.serializer(), prefs.getString(Constants.profilID, "")!!)

        val userPref = appContext.getSharedPreferences(Constants.userListFileName, 0)
        if (userPref.getString(Constants.userList, "") == "{}")
            User.contacts = ArrayList<User>()
        else
            User.contacts = User.getUsers(userPref.getString(Constants.userList, "")!!)

        val groupPref = appContext.getSharedPreferences(Constants.groupListFileName, 0)
        if (groupPref.getString(Constants.groupList, "") == "{}")
            Group.groupList = ArrayList<Group>()
        else
            Group.groupList = Group.getGroups(groupPref.getString(Constants.groupList, "")!!)
        CommunicationManager.comManager.launchReceiver(appContext, User.profil.name)
        val group = Intent(appContext, groupListControlleur::class.java)
        // start your next activity
        appContext.startActivity(group)
    }
}

