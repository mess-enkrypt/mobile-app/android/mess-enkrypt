package messnco.mess_enkrypt.models

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import kotlinx.serialization.stringify
import messnco.mess_enkrypt.viewControler.groupCreate

interface SharedPref {
    companion object {

        fun getIsLock(context: Context) : String {
            val pref = context.getSharedPreferences(Constants.profilFileName, 0)
            return pref.getString(Constants.isLock, "")!!
        }
        fun setIsLock(context: Context, act: String) {
            val prefs = context.getSharedPreferences(Constants.profilFileName, 0)
            //val isLockPref = prefs.getString(Constants.isLock, "")
            val editorLock = prefs.edit()
            editorLock.putString(Constants.isLock, act)
            editorLock.apply()
        }

        fun setFingerPrint(act: String, context: Context) {
            val pref = context.getSharedPreferences(Constants.prefsFileName, 0)
            val edit = pref.edit()
            edit.putString(Constants.useFingerPrint, act)
            edit.apply()
        }

        fun getFingerPrint(context: Context) : String {
            val pref = context.getSharedPreferences(Constants.prefsFileName, 0)
            val edit = pref.getString(Constants.useFingerPrint, "no")
            return edit!!
        }

        fun setIntentFingerPrint(num : Int, context: Context) {
            val pref = context.getSharedPreferences(Constants.prefsFileName, 0)
            val edit = pref.edit()
            edit.putInt(Constants.fingerPrintItent, num)
            edit.apply()
        }

        fun getIntentFingerPrint(context: Context) : Int {
            val pref = context.getSharedPreferences(Constants.prefsFileName, 0)
            val edit = pref.getInt(Constants.fingerPrintItent, 0)
            return edit!!
        }

        fun getUnlockPass(context: Context) : String {
            val pref = context.getSharedPreferences(Constants.profilFileName, 0)
            return pref.getString(Constants.unlockPass, "")!!
        }

        fun getDestroyPass(context: Context) : String {
            val pref = context.getSharedPreferences(Constants.profilFileName, 0)
            return pref.getString(Constants.destroyPass, "")!!
        }

        fun saveUnlockPass(context: Context, pass: String) {
            val pref = context.getSharedPreferences(Constants.profilFileName, 0)
            val edit = pref.edit()
            edit.putString(Constants.unlockPass, pass)
            edit.apply()
        }

        fun saveDestroyPass(context: Context, pass: String) {
            val pref = context.getSharedPreferences(Constants.profilFileName, 0)
            val edit = pref.edit()
            edit.putString(Constants.destroyPass, pass)
            edit.apply()
        }

        fun setIsUnlockPassSet(context: Context, act: String) {
            val pref = context.getSharedPreferences(Constants.profilFileName, 0)
            val edit = pref.edit()
            edit.putString(Constants.isUnlockPassSet, act)
            edit.apply()
        }

        fun setIsDestroyPassSet(context: Context, act: String) {
            val pref = context.getSharedPreferences(Constants.profilFileName, 0)
            val edit = pref.edit()
            edit.putString(Constants.isDestroyPassSet, act)
            edit.apply()
        }

        fun getIsUnlockPassSet(context: Context) : String {
            val pref = context.getSharedPreferences(Constants.profilFileName, 0)
            //Log.d("COO", pref.getBoolean(Constants.isUnlockPassSet, false).toString())
            return pref.getString(Constants.isUnlockPassSet, "false")!!
        }

        fun getIsDestroyPassSet(context: Context) : String {
            val pref = context.getSharedPreferences(Constants.profilFileName, 0)
            return pref.getString(Constants.isDestroyPassSet, "false")!!
        }

        @ImplicitReflectionSerializer
        fun saveContacts(context: Context) {
            val pref = context.getSharedPreferences(Constants.userListFileName, 0)
            val edit = pref.edit()
            edit.putString(Constants.userList, "{\"users\":" + Json.stringify(User.contacts)+"}")
            edit.apply()
        }

        @ImplicitReflectionSerializer
        fun saveGroups(context: Context) {
            val pref = context.getSharedPreferences(Constants.groupListFileName, 0)
            val edit = pref.edit()
            Log.d("COO", groupCreate.groupListToJson(Group.groupList))
//            val favGroupInstance = ArrayList<Group>()
//            val noFavGroupInstance = ArrayList<Group>()
//            Group.groupList.forEach {
//                if (it.isFav) {
//                    favGroupInstance.add(it)
//                } else {
//                    noFavGroupInstance.add(it)
//                }
//            }
            val groupInstance = Group.groupList
//            groupInstance.addAll(noFavGroupInstance)
            groupInstance.sortBy { isFavSelector(it) }
//            groupInstance.sortByDescending { selector(it)}

            edit.putString(Constants.groupList, groupCreate.groupListToJson(groupInstance))
            edit.apply()
        }

        @UnstableDefault
        fun saveUser(context: Context, pI: String) {
            val profilShare = context.getSharedPreferences(Constants.profilFileName, 0)

            val editor = profilShare.edit()

            editor.putString(Constants.pass, pI)
            editor.putString(Constants.profilID, Json.stringify(User.serializer(),User.profil))
            editor.apply()
        }

        fun saveKeys(context: Context, pbK: String, privKey: String) {
            val KeysPrefs: SharedPreferences = context.getSharedPreferences(Constants.keysFileName, 0)

            val keysEdit = KeysPrefs.edit()

            keysEdit.putString(Constants.privateKey, privKey)
            keysEdit.putString(Constants.publicKey, pbK)
            keysEdit.apply()
        }

        fun initContact(context: Context) {
            val userPref = context.getSharedPreferences(Constants.userListFileName, 0)
            val userPrefEdit = userPref.edit()
            userPrefEdit.putString(Constants.userList, "{}")
            userPrefEdit.apply()
        }

        fun initGroup(context: Context) {
            val groupPref = context.getSharedPreferences(Constants.groupListFileName, 0)
            val groupPrefEdit = groupPref.edit()
            groupPrefEdit.putString(Constants.groupList, "{}")
            groupPrefEdit.apply()

        }

        fun initSetting(context: Context) {
            val settingsPref = context.getSharedPreferences(Constants.settingsFileName, 0)
            val settingsPrefEdit = settingsPref.edit()
            settingsPrefEdit.putString(Constants.settings, "{}")
            settingsPrefEdit.apply()
        }

        fun clearSharedPref(context: Context) {
            context.getSharedPreferences(Constants.profilFileName, 0).edit().clear().apply()
            context.getSharedPreferences(Constants.keysFileName, 0).edit().clear().apply()
            context.getSharedPreferences(Constants.userListFileName, 0).edit().clear().apply()
            context.getSharedPreferences(Constants.groupListFileName, 0).edit().clear().apply()
            context.getSharedPreferences(Constants.prefsFileName, 0).edit().clear().apply()
            context.getSharedPreferences(Constants.settingsFileName, 0).edit().clear().apply()
        }

        fun selector(g: Group): Long{
            Log.d("COO", g.messages.toString())
            if (g.messages.size > 1) {
                val lol = g.messages.last()
                return lol.createAt
            } else {
                return 3L
            }
            //return lol
        }

        fun isFavSelector(g: Group) : Boolean {
            return !g.isFav
        }

        fun getGroupPositionById(id: Int) : Int {
            var res = -1
            var cpt = 0
            Group.groupList.forEach {
                if (id == it.id) {
                    res = cpt
                }
                cpt++
            }
            return res
        }

    }
}