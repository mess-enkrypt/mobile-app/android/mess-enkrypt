package messnco.mess_enkrypt.models
import android.content.Context
import android.content.Intent
//import android.support.v4.content.ContextCompat.startActivity
import android.util.Log
import kotlinx.android.synthetic.main.fragment_add_user_friend.*
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.stringify
import messnco.mess_enkrypt.viewControler.userControlleur
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


@Serializable
class User(

        var name: String = "",
        var id: Int = 0,
        var imageUrl: String = "",
        var publicKey: String = "noPublicKey",
        var trustLevel: Int = 0, // MAX = 5, MIN = 0 echelle de droit a prévoir
        var passPhrase: String = ""
        ) {

    companion object  {

        var profil : User = User()
        var contacts : ArrayList<User> = ArrayList<User>()

        fun deleteUser(username: User) {
            contacts.remove(username)
        }

        @ImplicitReflectionSerializer
        fun addUser(username: String, context: Context) {
            Log.d("COO", "Before Emitting")
            Log.d("COO", username)
            CommunicationManager.comManager.h.emit("user:getPublicKey", username)
//                    .on("user:getPublicKey:success", {
//                        Log.d("test", "on receiving GetPublicKey")
//                        val receiveMSG = JSONArray(it)
//                        val JSONdata = JSONObject(receiveMSG[0].toString())
//                        val strToSave = JSONdata.getString("publicKey")
//                        Log.d("COO", strToSave)
//                        val newUser = User(username, 1, "", strToSave, 0, "")
//
//                        for (x in 0 until User.contacts.size){
//                            if (newUser.name == User.contacts[x].name && newUser.publicKey == User.contacts[x].publicKey) {
//                                throw IllegalArgumentException("User Already Exist")
//                            }
//                        }
//
//
//                        User.contacts.add(newUser)
//                        val pref = context.getSharedPreferences(Constants.userListFileName, 0)
//                        val edit = pref.edit()
//                        edit.putString(Constants.userList, "{\"users\":" + Json.stringify(User.contacts)+"}")
//                        edit.apply()
//                    })
//                    .on("user:getPublicKey:err", {
//                        val receiveMSG = JSONArray(it)
//                        val strToSave = receiveMSG[0].toString()
//                        if (strToSave == "USER_NOT_ACTIVE")
//                        {
//                            Log.d("COO", strToSave)
//                        } else if (strToSave == "USER_NOT_FOUND") {
//                            Log.d("COO", strToSave)
//                        }
//                    })

        }

        fun getUsers(JSONList: String): ArrayList<User> {
            val usersList = ArrayList<User>()

            try {
                // Load data
                val json = JSONObject(JSONList)
                val users = json.getJSONArray("users")

                // Get Recipe objects from data
                (0 until users.length()).mapTo(usersList) {
                    User(users.getJSONObject(it).getString("name"),
                            users.getJSONObject(it).getInt("id"),
                            users.getJSONObject(it).getString("imageUrl"),
                            users.getJSONObject(it).getString("publicKey"),
                            users.getJSONObject(it).getInt("trustLevel"),
                            "")
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return usersList
        }

        private fun loadJsonFromAsset(filename: String, context: Context): String? {
            val json: String?

            try {
                val inputStream = context.assets.open(filename)
                val size = inputStream.available()
                val buffer = ByteArray(size)
                inputStream.read(buffer)
                inputStream.close()
                json = String(buffer, Charsets.UTF_8)
            } catch (ex: java.io.IOException) {
                ex.printStackTrace()
                return null
            }

            return json
        }
    }
}
