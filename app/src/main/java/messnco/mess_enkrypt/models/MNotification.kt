package messnco.mess_enkrypt.models

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import kotlinx.serialization.ImplicitReflectionSerializer
import messnco.mess_enkrypt.R
import messnco.mess_enkrypt.viewControler.MainActivity
import messnco.mess_enkrypt.viewControler.groupListControlleur
import messnco.mess_enkrypt.viewControler.groupMainFragment
import java.util.concurrent.ThreadLocalRandom

class MNotification {



    companion object {
        var isInit: String = "no"
        lateinit var notificationManager: NotificationManager
        lateinit var notificationChanel: NotificationChannel
        lateinit var notificationBuilder: Notification.Builder
        lateinit var builderSummary: Notification.Builder
        private val channelId = "messnco.mess_enkrypt.models"
        private val description = "Messages"
        val GROUP_KEY_NOTIFY = "group_key_notify"

        fun initNotif(context: Context) {
            isInit = "yes"
            notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationChanel = NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
                notificationChanel.enableLights(true)
                notificationChanel.lightColor = Color.RED
                notificationChanel.enableVibration(true)
                notificationManager.createNotificationChannel(notificationChanel)
                builderSummary = Notification.Builder(context, channelId)
                        .setSmallIcon(R.drawable.ic_launcher_round)
                        .setContentTitle("MessEnkrypt")
                        .setContentText("You have new messages")
                        .setGroup(GROUP_KEY_NOTIFY)
                        .setGroupSummary(true)
            } else {
                builderSummary = Notification.Builder(context)
                        .setContentTitle("MessEnkrypt")
                        .setContentText("You have new messages")
                        .setSmallIcon(R.drawable.ic_launcher_round)
                        .setGroup(GROUP_KEY_NOTIFY)
                        .setGroupSummary(true)
            }
            //notificationManager.notify(0, builderSummary.build())
        }

        @ImplicitReflectionSerializer
        fun createNotifMessage(context: Context, msg: String) {
            //notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val intent = Intent(context, groupListControlleur::class.java)
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                notificationChanel = NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
//                notificationChanel.enableLights(true)
//                notificationChanel.lightColor = Color.RED
//                notificationChanel.enableVibration(true)
//                notificationManager.createNotificationChannel(notificationChanel)
                notificationBuilder = Notification.Builder(context, channelId)
                        .setContentTitle("New message !")
                        .setContentText(msg)
                        .setSmallIcon(R.drawable.ic_launcher_round)
                        .setContentIntent(pendingIntent)
                        .setGroup(GROUP_KEY_NOTIFY)
            } else {
                notificationBuilder = Notification.Builder(context)
                        .setContentTitle("New message !")
                        .setContentText(msg)
                        .setSmallIcon(R.drawable.ic_launcher_round)
                        .setContentIntent(pendingIntent)
                        .setGroup(GROUP_KEY_NOTIFY)

            }
            val randomInteger = ThreadLocalRandom.current().nextInt()
            notificationManager.notify(0, builderSummary.build())
//            notificationManager.notify(randomInteger, notificationBuilder.build())
        }

        @ImplicitReflectionSerializer
        fun createNotifGroupe(context: Context, msg: String) {
//            notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val intent = Intent(context, groupListControlleur::class.java)
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                notificationChanel = NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
//                notificationChanel.enableLights(true)
//                notificationChanel.lightColor = Color.RED
//                notificationChanel.enableVibration(true)
//                notificationManager.createNotificationChannel(notificationChanel)

                notificationBuilder = Notification.Builder(context, channelId)
                        .setContentTitle("New group !")
                        .setContentText(msg)
                        .setSmallIcon(R.drawable.ic_launcher_round)
                        .setContentIntent(pendingIntent)
                        .setGroup(GROUP_KEY_NOTIFY)

            } else {
                notificationBuilder = Notification.Builder(context)
                        .setContentTitle("New group !")
                        .setContentText(msg)
                        .setSmallIcon(R.drawable.ic_launcher_round)
                        .setContentIntent(pendingIntent)
                        .setGroup(GROUP_KEY_NOTIFY)

            }
            val randomInteger = ThreadLocalRandom.current().nextInt()
            notificationManager.notify(0, builderSummary.build())
//            notificationManager.notify(randomInteger, notificationBuilder.build())
        }

    }
}