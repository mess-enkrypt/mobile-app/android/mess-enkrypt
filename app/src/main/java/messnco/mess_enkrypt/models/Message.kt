package messnco.mess_enkrypt.models

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.json.JSONException
import org.json.JSONObject

@Serializable
class MMessage (
    var ID : Int = 0,
    var content : String = "",
    var createAt : Long = 0,
    var sender : User = User(),
    var isMe : Boolean = true,
    var picture : String = "")
{

    companion object {

        fun getMessages(JSONList: String): ArrayList<MMessage> {
            val messageList = ArrayList<MMessage>()

            try {
                // Load data
                val json = JSONObject(JSONList)
                val mess = json.getJSONArray("messages")

                // Get Recipe objects from data
                (0 until mess.length()).mapTo(messageList) {
                    MMessage(
                            mess.getJSONObject(it).getInt("ID"),
                            mess.getJSONObject(it).getString("content"),
                            mess.getJSONObject(it).getLong("createAt"),
                            Json.parse(User.serializer(), mess.getJSONObject(it).getString("sender").toString()),
                            mess.getJSONObject(it).getBoolean("isMe"),
                            mess.getJSONObject(it).getString("picture")
                    )
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return messageList
        }
    }
}
